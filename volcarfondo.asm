;///////////////////////////////////////////////////////////////////////////
;                Volcados de fondo del enemigo desde el buffer

; ENTRADA DE RUTINA HL=VALOR DE LA POSICION DEL SPRITE EN EL BUFFER DE PANTALLA
; DE= VALOR DEL SPRITE EN PANTALLA Y BC = TAMA�O DEL SPRITE EN COLUMNAS
; TENED EN CUENTA LOS SCANNES DE ALTO POR SUPUESTO. HAY QUE SALTAR CADA 32
; DE=DESTINO EN PANTALLA. HL=24580+POSIC SPRITET. BC=3 HACER BUCLE PARA 3X8 = 24 SCANNES
VOL_FON_ENE_VER:
            LD A,(IX+14)      ;Traspaso a HL la pos.del sprite en buffer
            LD L,A            ;calculado por C_DIRBUFF_SPRITE cuando
            LD A,(IX+15)      ;cambias de posicion (x,y)en GESTORENE.ASM
            LD H,A
            LD A,(IX+12)      ;Traspaso a DE la pos.del sprite en pantalla
            LD E,A            ;calculado por TABLA4/256 cuando
            LD A,(IX+13)      ;cambias de posicion (x,y)en GESTORENE.ASM
            LD D,A
;HL = Inicio del buffer y DE = A la posicion en el archivo de pantalla
            LD C,48          ;C= mismo valor en horiz.que vertical 2x3x8
            LD B,0          ; o 3x3x8
SPOTRALI_V  LDI
            LDI
            LD A,C
            AND A
            RET Z
            LD C,30          ;;;PARA SACAR JUSTO EL SCANN DE DEBAJO LA PROXIMA PASADA
            ADD HL,BC         ;;;
            LD C,A
            DEC   E
            DEC   E
            call fin_vol_fondo_ene
            jr SPOTRALI_V
VOL_FON_ENE_HOR:
            LD A,(IX+14)      ;Traspaso a HL la pos.del sprite en buffer
            LD L,A            ;calculado por C_DIRBUFF_SPRITE cuando
            LD A,(IX+15)      ;cambias de posicion (x,y)en GESTORENE.ASM
            LD H,A
            LD A,(IX+12)      ;Traspaso a DE la pos.del sprite en pantalla
            LD E,A            ;calculado por TABLA4/256 cuando
            LD A,(IX+13)      ;cambias de posicion (x,y)en GESTORENE.ASM
            LD D,A
            LD C,48          ;Mismo valor en horiz. que vertical
            LD B,0
SPOTRALI_H  LDI
            LDI
            LDI
            LD A,C
            AND A
            RET Z
            LD C,29          ;;;PARA SACAR JUSTO EL SCANN DE DEBAJO LA PROXIMA PASADA
            ADD HL,BC         ;;;
            LD C,A
            DEC   E
            DEC   E
            DEC   E
            call fin_vol_fondo_ene
            jr SPOTRALI_H
;///////////////////////////////////////////////////////////////////////////
;                Partes comunes de las rutinas
;Principio de la rutina
cab_vol_fondo_ene:
            LD A,(IX+14)      ;Traspaso a HL la pos.del sprite en buffer
            LD L,A            ;calculado por C_DIRBUFF_SPRITE cuando
            LD A,(IX+15)      ;cambias de posicion (x,y)en GESTORENE.ASM
            LD H,A
            LD A,(IX+12)      ;Traspaso a DE la pos.del sprite en pantalla
            LD E,A            ;calculado por TABLA4/256 cuando
            LD A,(IX+13)      ;cambias de posicion (x,y)en GESTORENE.ASM
            LD D,A
            ret
;Final de la rutina
fin_vol_fondo_ene:
            INC   D
            LD    A,D
            AND   7
            ret   NZ          ;baja una l�nea dentro del caracter
            LD    A,#20
            ADD   A,E
            LD    E,A
            ret   C           ;cambio de tercio
            LD    A,D
            SUB   8
            LD    D,A
            ret               ;cambio de caracter
;///////////////////////////////////////////////////////////////////////////
;                Volcados de trozos del fondo del enemigo.

;_______________  Vuelca solo un tile de 1 ancho x 2 alto
VOL_FON_ENE_H_C:
            call cab_vol_fondo_ene
            LD C,16          ;Mismo valor en horiz. que vertical
            LD B,0
SPOTRALI_HC LDI
            LD A,C
            AND A
            RET Z
            LD C,31          ;;;PARA SACAR JUSTO EL SCANN DE DEBAJO LA PROXIMA PASADA
            ADD HL,BC         ;;;
            LD C,A
            DEC   E
            call fin_vol_fondo_ene
            jr SPOTRALI_HC
;_______________  Vuelca solo un tile de 2 ancho x 1 alto
VOL_FON_ENE_V_C:
            call cab_vol_fondo_ene
            LD C,16          ;C= mismo valor en horiz.que vertical 2x3x8
            LD B,0          ; o 3x3x8
SPOTRALI_VC LDI
            LDI
            LD A,C
            AND A
            RET Z
            LD C,30          ;;;PARA SACAR JUSTO EL SCANN DE DEBAJO LA PROXIMA PASADA
            ADD HL,BC         ;;;
            LD C,A
            DEC   E
            DEC   E
            call fin_vol_fondo_ene
            jr SPOTRALI_VC
;_______________  Vuelca fondo tile de 2 ancho x 2 ancho
VOL_FON_ENE_DISPARO:
            call cab_vol_fondo_ene
            LD C,32          ;C= mismo valor en horiz.que vertical 2x3x8
            LD B,0          ; o 3x3x8
SPOTRALI_D  LDI
            LDI
            LD A,C
            AND A
            RET Z
            LD C,30          ;;;PARA SACAR JUSTO EL SCANN DE DEBAJO LA PROXIMA PASADA
            ADD HL,BC         ;;;
            LD C,A
            DEC   E
            DEC   E
            call fin_vol_fondo_ene
            jr SPOTRALI_D
;_______________  Vuelca solo un tile de 1 ancho x 3 alto
VOL_FON_ENE_H_IAC:
            call cab_vol_fondo_ene
            LD C,24          ;Mismo valor en horiz. que vertical
            LD B,0
SPOTRALI_HI LDI
            LD A,C
            AND A
            RET Z
            LD C,31          ;;;PARA SACAR JUSTO EL SCANN DE DEBAJO LA PROXIMA PASADA
            ADD HL,BC         ;;;
            LD C,A
            DEC   E
            call fin_vol_fondo_ene
            jr SPOTRALI_HI
