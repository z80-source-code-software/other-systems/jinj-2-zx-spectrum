;RUTINA DE CALCULO DE LA DIRECCION EN EL BUFFER INTERMEDIO DADAS LA FILA Y LA COLUMNA
;ENTRADA:FILA pixels Y COLUMNA en chars. Pasar pixels a chars
;SALIDA:POSICION DE INICIO DEL SPRITE EN EL BUFFER DE PANTALLA
;)
C_DIRBUFF:
            xor a
            LD   A,(COLU)
            LD   E,A
            LD   A,(FILA)
            LD   D,A      ;DE = fila*32+columna
            LD   HL,45391-1536  ;1536 = 6 chars alto x 32 ancho x 8 bytes  Marcador superior
            ADD  HL,DE   ;TOTAL
            RET

;ENTRADA:fila y colu en chars del prota. Para imprimir attr tras �l.
;SALIDA:POSICION DE INICIO DE LOS ATTR DEL SPRITE EN EL BUFFER

C_DIRBUFFSPATT:
            LD   HL,49487  ;antes Inicio buffer attr
            LD   A,(FILA)
            sub 6
            JR   Z,SUMACOLUASP
            LD   D,0
            LD   E,A
;PARA CALCULAR LOS CHARS DEBEMOS 1� DIVIDIR ENTRE 8 LOS SCANNES FILA Y DESPUES
;MULTIPLICAR POR 32 (CADA FILA TIENE 32) ES LO MISMO QUE SI MULTIPLICAMOS 4 DE PRIMERAS.
            SLA E         ;MULTIPLICAMOS POR 4 (DOS VECES CORRER A LA IZQ. DE
            RL D
            SLA E
            RL D
            SLA E
            RL D
            SLA E
            RL D
            SLA E
            RL D
            ADD  HL,DE    ;YA TENEMOS EL VALOR DE LA FILA SUMADO
SUMACOLUASP
            ld a,(COLU)
            LD   E,A
            LD   D,0
            ADD  HL,DE   ;SUMO LA COLUMNA
            RET
;TENGO EN HL LA POSICION EN EL BUFFER DE ATTR DEL FONDO (Que de momento siempre es 1)