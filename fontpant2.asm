; ASM source file created by SevenuP v1.12
; SevenuP (C) Copyright 2002-2004 by Jaime Tejedor G�mez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16,   8)
;Char Size:       (  2,   1)
;Sort Priorities: X char, Char line
;Attributes:      At the end
;Mask:            No
;         org 32768
;----------------------------------------------------------------
;       fila x columnas
        ORG 32671
       DEFB 22,48,0
       DEFB 22,48,8
       DEFB 22,48,16
       DEFB 22,48,28
       DEFB 23,80,0
       DEFB 23,80,8
       DEFB 22,80,20
       DEFB 22,80,24
       DEFB 22,112,4
       DEFB 22,112,12
       DEFB 23,112,20
       DEFB 22,112,28
       DEFB 22,144,0
       DEFB 22,144,8
       DEFB 22,144,16
       DEFB 22,144,20
       DEFB 23,48,4
       DEFB 23,48,12
       DEFB 23,48,20
       DEFB 23,48,24
       DEFB 22,80,4
       DEFB 22,80,12
       DEFB 23,80,16
       DEFB 23,80,28
       DEFB 23,112,0
       DEFB 23,112,8
       DEFB 22,112,16
       DEFB 23,112,24
       DEFB 23,144,4
       DEFB 23,144,12
       DEFB 23,144,24
       DEFB 23,144,28
       DEFB 128
