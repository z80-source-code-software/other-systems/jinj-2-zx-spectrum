; ASM source file created by SevenuP v1.12
; SevenuP (C) Copyright 2002-2004 by Jaime Tejedor G�mez, aka Metalbrain

;----------------------------------------------------
;       altura en scannes y anchura en caracteres
;
;----------------------------------------------------
        ORG 24177
TABDATA

;FONDOS DE PANTALLA
;suelo 1
        DEFB 0,32,4
	DEFB	 20, 86, 40,160
	DEFB	 32,  1,  0,  0
	DEFB	  0,  4, 64, 16
	DEFB	 64,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	 64,  0,  0,  8
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  8

	DEFB	 84,  0,  0,  0
	DEFB	128,  0,  0,  0
	DEFB	  2,  0,  0, 16
	DEFB	128,128,  0,  0
	DEFB	  2,  0,  0,  0
	DEFB	  0,128,  0, 32
	DEFB	128, 42,128,  0
	DEFB	 32,  0, 21,  0

	DEFB	  2,  0,  0,  0
	DEFB	 20,  0, 84, 72
	DEFB	  0,  0,130,  2
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,128,  0
	DEFB	  8,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	 16,  0,  0,  0

	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	 16,  0,  0,  0
	DEFB	  0,  0, 32,  1
	DEFB	  0,  0,  0,  0
	DEFB	  4,129, 16,  1
	DEFB	  0, 32, 20,  0
	DEFB	  0, 18,  0,149
; (Attributes here, insert label if needed)
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1

;suelo 2
        DEFB 1,32,4
	DEFB	  0,  0,  0,  0
	DEFB	  0, 18, 64,  0
	DEFB	 32,  0,  0,  1
	DEFB	  0,  0, 32,  0
	DEFB	 64,  0,  0,  1
	DEFB	  0,  0,  0,144
	DEFB	  0,  0, 64,  0
	DEFB	  0,  2,  0,  4

	DEFB	 80, 16,  0,  0
	DEFB	  0,  0,  0,  2
	DEFB	  9,  0,  0,  0
	DEFB	  0,  0,128,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,130,  2
	DEFB	 20,  0, 84, 72
	DEFB	  2,  0,  0,  0

	DEFB	 32,128, 21,  0
	DEFB	128,  0,  0,  0
	DEFB	  0, 32, 64, 32
	DEFB	  0,  0,  0,  0
	DEFB	128,  0,  0,  0
	DEFB	  0,  5,  0, 16
	DEFB	128,  0,  0,  0
	DEFB	 80,  0,  0,  0

	DEFB	  0, 16,  0,  8
	DEFB	  0,  0,  0,  0
	DEFB	 64,  1,  0,  8
	DEFB	  0, 32,  0,  0
	DEFB	 64,  4,  0,  0
	DEFB	  0,  0,  0, 16
	DEFB	 33,  0,128,  0
	DEFB	 21,  0, 42,160
; (Attributes here, insert label if needed)
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1

;flor 2
        DEFB 2,16,2
	DEFB	 68,  0
	DEFB	174,  0
	DEFB	 68, 48
	DEFB	 40,120
	DEFB	 26,108
	DEFB	 37,180
	DEFB	114, 52
	DEFB	 32, 88

	DEFB	  3,114
	DEFB	122,220
	DEFB	 14,192
	DEFB	 11,144
	DEFB	  1,124
	DEFB	  3,192
	DEFB	 45,144
	DEFB	 19, 56
; (Attributes here, insert label if needed)
	DEFB	 69,  2
	DEFB	 68,  4


;rejas
        DEFB 3,16,2
	DEFB	192,192
	DEFB	192,192
	DEFB	225,225
	DEFB	  0,  0
	DEFB	 76, 76
	DEFB	140,140
	DEFB	 94, 94
	DEFB	  0,  0

	DEFB	 37,140
	DEFB	108,196
	DEFB	228,237
	DEFB	 45,  4
	DEFB	196, 76
	DEFB	204,100
	DEFB	132,109
	DEFB	140,101
; (Attributes here, insert label if needed)
	DEFB	 69, 68
	DEFB	  4,  4

;calavera
        DEFB 4,8,1
	DEFB	 56,124,222,138,222,116, 42,  0
; (Attributes here, insert label if needed)
	DEFB	 71

;flor1
        DEFB 5,16,2
	DEFB	  0, 34
	DEFB	  0,119
	DEFB	  8,255
	DEFB	 30,148
	DEFB	 38, 91
	DEFB	 45,231
	DEFB	 24,254
	DEFB	  2, 76

	DEFB	 14,192
	DEFB	 27, 76
	DEFB	 35,126
	DEFB	  9,214
	DEFB	 62,128
	DEFB	 99,204
	DEFB	 57,178
	DEFB	 20,128
; (Attributes here, insert label if needed)
	DEFB	 67, 66
	DEFB	 68,  4

; ladrillo
        DEFB 6,16,2
	DEFB	  0,  0
	DEFB	 61,234
	DEFB	 91,253
	DEFB	101,250
	DEFB	127,253
	DEFB	127,250
	DEFB	127,213
	DEFB	 58,170

	DEFB	  0,  0
	DEFB	234, 21
	DEFB	253, 47
	DEFB	250, 95
	DEFB	253, 15
	DEFB	250, 85
	DEFB	245, 42
	DEFB	106, 21
; (Attributes here, insert label if needed)
	DEFB	  3, 66
	DEFB	 66,  2

;lapida1
        DEFB 7,16,2
	DEFB	 15,  0
	DEFB	 28,192
	DEFB	 40, 32
	DEFB	 82, 32
	DEFB	162,144
	DEFB	227,136
	DEFB	167,  8
	DEFB	229,  4

	DEFB	161,132
	DEFB	 96,140
	DEFB	 96, 22
	DEFB	116, 12
	DEFB	118,150
	DEFB	 63, 42
	DEFB	111,124
	DEFB	  5,192
; (Attributes here, insert label if needed)
	DEFB	 66,  2
	DEFB	 66, 67

;lapida2
        DEFB 8,16,2
	DEFB	  1,  0
	DEFB	  3,128
	DEFB	  5, 64
	DEFB	 15,224
	DEFB	  5, 64
	DEFB	  1,  0
	DEFB	  1,  0
	DEFB	  1,  0

	DEFB	  1,128
	DEFB	  6,160
	DEFB	 27, 80
	DEFB	 21,168
	DEFB	 59, 92
	DEFB	 31,176
	DEFB	  1,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	 71,  7
	DEFB	 66,  2


; abeto
        DEFB 9,16,2
; (Insert graphic label here if needed)

	DEFB	  3,160
	DEFB	 15, 80
	DEFB	 15,232
	DEFB	 31,208
	DEFB	 31,232
	DEFB	 31, 80
	DEFB	 14,168
	DEFB	  5, 80

	DEFB	  1, 96
	DEFB	  1,128
	DEFB	  1,  0
	DEFB	  7,128
	DEFB	  3,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	 68,  4
	DEFB	  3,  2

; arbolgrande
        DEFB 10,32,4
	DEFB	  0,  0,  0,  0
	DEFB	  0, 24,231,  0
	DEFB	  1,127,253,160
	DEFB	  5,255,250, 88
	DEFB	 11,255,253,104
	DEFB	 21,255,254,148
	DEFB	 91,255,253, 10
	DEFB	 47,255,254,134

	DEFB	 95,255,253, 66
	DEFB	191,255,254,129
	DEFB	 95,255,253, 66
	DEFB	191,255,254,129
	DEFB	 95,255,245, 66
	DEFB	191,255,250,129
	DEFB	 95,255,245, 66
	DEFB	191,255,234,  5

	DEFB	 95,255, 84, 10
	DEFB	175,254,170, 20
	DEFB	 87,253, 64, 40
	DEFB	 47,254,  0, 20
	DEFB	 95,253,  0, 40
	DEFB	 43,254,  5, 96
	DEFB	 29,253,170,128
	DEFB	  3,254, 84, 64

	DEFB	 77,251, 55,120
	DEFB	 14,255,254, 12
	DEFB	 54,114,189,192
	DEFB	 56, 79,233,160
	DEFB	 16,  4, 64,240
	DEFB	  0, 15,128, 56
	DEFB	  0,  7,128, 32
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	 68, 68, 68,  4
	DEFB	 68, 68, 68,  4
	DEFB	 68, 68,  4,  4
	DEFB	 70,  6,  6,  6

; matafabes
        DEFB 11,16,2
	DEFB	  0,  1
	DEFB	  0,  2
	DEFB	 14, 34
	DEFB	 49,203
	DEFB	 32,102
	DEFB	 64, 52
	DEFB	 64, 52
	DEFB	  0, 24

	DEFB	  0, 20
	DEFB	  4, 24
	DEFB	  0, 72
	DEFB	  0, 21
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	 68, 68
	DEFB	  1,  2

; piquetas
        DEFB 12,8,4
	DEFB	  3,192,  3,192
	DEFB	  7,224,  7,224
	DEFB	247,239,247,239
	DEFB	 89,149, 89,149
	DEFB	  2, 64,  2, 64
	DEFB	  3,192,  3,192
	DEFB	  3,192,  3,192
	DEFB	  1,128,  1,128
; (Attributes here, insert label if needed)
	DEFB	  2,  2,  2,  2

; puertas trampilla. Arbolgrande etc.
        DEFB 13,8,2
	DEFB	  0,  0
	DEFB	 27,216
	DEFB	 59,204
	DEFB	 91,218
	DEFB	 90, 74
	DEFB	 90, 88
	DEFB	 91,202
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	 66,  2


; entrada arbol
        DEFB 14,8,2
	DEFB	  0,  0
	DEFB	 30,  0
	DEFB	 32,  4
	DEFB	127, 82
	DEFB	 64,  6
	DEFB	254,171
	DEFB	253, 85
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  1,  1

;notamusical
        DEFB 15,16,2
	DEFB	  0,212
	DEFB	  0,162
	DEFB	  0,149
	DEFB	  0,154
	DEFB	  0,137
	DEFB	  1,132
	DEFB	  1,141
	DEFB	  1,136

	DEFB	 29, 16
	DEFB	 35,128
	DEFB	 71, 64
	DEFB	 79,128
	DEFB	111, 64
	DEFB	 62,128
	DEFB	  1, 64
	DEFB	 10,128
; (Attributes here, insert label if needed)
	DEFB	 1,1
	DEFB	 1,1

;fondo
        DEFB 16,16,2
fon2x2:                    ;esta etiqueta es para comptunotas.asm
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  4,144
	DEFB	  0,  0

	DEFB	 16,  4
	DEFB	  0,  0
	DEFB	  8, 64
	DEFB	  2,  4
	DEFB	 64,  0
	DEFB	 16, 16
	DEFB	  2, 64
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1

; muroinfierno
        DEFB 17,16,2
	DEFB	  8,176
	DEFB	 92, 98
	DEFB	169, 75
	DEFB	245,181
	DEFB	210,206
	DEFB	103,133
	DEFB	141,162
	DEFB	231,125

	DEFB	 84,211
	DEFB	168,169
	DEFB	 69, 92
	DEFB	 35, 86
	DEFB	116,136
	DEFB	210,162
	DEFB	169, 85
	DEFB	 82,242
; (Attributes here, insert label if needed)
	DEFB	 67, 67
	DEFB	 67,  3

; poste cementerio
        defb 18,16,1
	DEFB	 28, 58, 60, 58, 20, 34, 60, 24
	DEFB	 99,117,114,181,178,217, 34, 60
; (Attributes here, insert label if needed)
	DEFB	 71,  3

; valla cementerio
        defb 19,8,4
	DEFB	255,255,255,255
	DEFB	 85, 85, 85, 85
	DEFB	  0,  0,  0,  0
	DEFB	 74, 74, 74, 74
	DEFB	 74, 74, 74, 74
	DEFB	107,107,107,107
	DEFB	 72, 66, 10, 10
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	  3,  3,  3,  3

; valla inclinada cementerio
        DEFB 20,16,1
	DEFB	  3,  7, 14, 28, 58,114,232,202
	DEFB	160, 40,136,160, 32,128,128,  0
; (Attributes here, insert label if needed)
	DEFB	  3,  3


;Limpiafondos y borrar vidas
        defb 21,32,4
borra_jonhy
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0

	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0

	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0

	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0

;fondo infierno 1
        defb 22,32,4
	DEFB	 12,  0,  2, 48
	DEFB	 80, 64, 32, 10
	DEFB	 80,  1,  2, 10
	DEFB	 16,128,  0,  8
	DEFB	 32,  0,  1,  4
	DEFB	193, 15,  0,  3
	DEFB	  0, 16,224,128
	DEFB	  2, 16, 24, 64

	DEFB	  8, 33,  8,  8
	DEFB	160, 42,  8,  2
	DEFB	  0,  2,  4,  0
	DEFB	  0,242,132,  0
	DEFB	  1, 12,  8,  0
	DEFB	 66, 66, 17,  0
	DEFB	  4, 17, 32,128
	DEFB	  4,  2,192,  4

	DEFB	 36,  4,  0,128
	DEFB	  4,  4,  0, 64
	DEFB	  2,  0,  0,  2
	DEFB	  2, 16,  0, 64
	DEFB	  1, 64,  0,  0
	DEFB	  1,  0,  0,128
	DEFB	160, 32,  0,  2
	DEFB	  8, 16,  0,  8

	DEFB	  2,  0,  8, 32
	DEFB	193,  5, 80,131
	DEFB	 48,  0,128, 12
	DEFB	  8,128,  1, 16
	DEFB	  4,  0,  1, 32
	DEFB	  2, 64,128, 64
	DEFB	  2,  4,  2, 64
	DEFB	226, 64,  0, 71
;atr
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1

; fondo infierno 2
        defb 23,32,4
	DEFB	 12,  0,  2, 48
	DEFB	 80, 64, 32, 10
	DEFB	 80,  1,  2, 10
	DEFB	 16,128,  0,  8
	DEFB	 32,  0,  1,  4
	DEFB	193,  0,  0,  3
	DEFB	  0,  0,  0,128
	DEFB	  2,  0,  0, 64

	DEFB	  8,  7,224,  8
	DEFB	160, 24, 24,  2
	DEFB	  0, 36, 36,  0
	DEFB	  0, 88, 26,  0
	DEFB	  0, 67,194,  0
	DEFB	 64,136, 21,  0
	DEFB	  0,136, 21,  0
	DEFB	  0,199,227,  4

	DEFB	 32,128,  1,  0
	DEFB	  0,191,253,  0
	DEFB	  0,192,  3,  2
	DEFB	  0, 10, 80,  0
	DEFB	  0, 18, 72,  0
	DEFB	  0, 16,  8,  0
	DEFB	160, 16,  8,  2
	DEFB	  8, 15,240,  8

	DEFB	  2,  0,  0, 32
	DEFB	193,  0,  0,131
	DEFB	 48,  0,  0, 12
	DEFB	  8,128,  1, 16
	DEFB	  4,  0,  1, 32
	DEFB	  2, 64,128, 64
	DEFB	  2,  4,  2, 64
	DEFB	226, 64,  0, 71
; (Attributes here, insert label if needed)

	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1

; tuberia de Mario.
        defb 24,16,2

	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  3,192
	DEFB	 13,112
	DEFB	 50,172
	DEFB	 96, 22
	DEFB	 80, 12

	DEFB	 79,242
	DEFB	112,  4
	DEFB	 63,250
	DEFB	 63,164
	DEFB	 15, 80
	DEFB	  0,  0
	DEFB	 13, 40
	DEFB	  5, 96
;attr
	DEFB	  1,  1
	DEFB	  1,  1
