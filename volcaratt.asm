VOL_CARATT:
        LD    DE,#58C0
        LD    HL,49487
        LD    BC,512
        LDIR
        RET
CINCOLINEAS:              ;HL tiene la direccion en el buffer de atributos.
        PUSH HL           ;Guardo el valor de HL buffer attr
        CALL IMP_AT
        EX DE,HL          ;Interc.valores DE tiene el valor en el arch.attr
        POP HL            ;Saco el valor del buffer de attr.
        RET
VOLCARSPAT:  ;Vuelca los atrib. del fondo del sprite al moverse en hor.
             ;esto se llama desde moviments.asm
        call CINCOLINEAS
        LD A,(HL)
        LD (DE),A
        ld bc,#20
        add hl,bc
        push hl
        ld hl,#20
        add hl,de
        push hl
        pop de
        pop hl
        LD A,(HL)       ;segunda fila
        LD (DE),A
        RET
VOLCARSPATVE:  ;Vuelca los atributos del fondo del sprite al moverse vert.
        call CINCOLINEAS
        LD A,(ANCHO)       ;DEL PROTA EN ESTE MOMENTO.
        LD B,A
BVARSPV LD A,(HL)
        LD (DE),A
        INC HL
        INC DE
        DEC B
        JR NZ,BVARSPV
        RET
VOLCARSPATTOTAL:  ;Vuelca los atributos del fondo del sprite total
                  ;se llama desde dirsenpro.asm
        call CINCOLINEAS
        LD A,(ALTO)
        LD B,A
BVARSPB PUSH BC
        PUSH HL
        PUSH DE
        LD B,0
        LD A,(ANCHO)       ;DEL PROTA EN ESTE MOMENTO.
        LD C,A
        LDIR
        POP DE
        POP HL
        ld bc,#20
        add hl,bc
        push hl
        ld hl,#20
        add hl,de
        push hl
        pop de
        pop hl
        POP BC
        DEC B
        JR NZ,BVARSPB
        RET