;---------------------------------------------------------------------------
;JINJ II - Belmonte's Revenge
;Perpetrador - Jos� J. R�denas
;---------------------------------------------------------------------------

        INCLUDE "FONTPANT.ASM"       ;24080
        INCLUDE "DATAS_TILE.ASM"     ;24177
        INCLUDE "DATMEN.ASM"         ;25101
        INCLUDE "MENUINFIZQ.ASM"     ;26154
        INCLUDE "MENUINFDER.ASM"     ;26514
        INCLUDE "LETRACONTROLES2.ASM" ;26874
        INCLUDE "IMPMENU.ASM"        ;27879
        INCLUDE "IMPMARCA.ASM"       ;28026
        INCLUDE "PINTARNOTA.ASM"     ;30200
        INCLUDE "MENUSUPER.ASM"      ;30585
        INCLUDE "TORBELLINO.ASM"
        INCLUDE "IMPPINTERM.ASM"     ;31699
        include "DESTELLO.ASM"       ;31747      ;fin en 31840 a 31902=62bytes
        include "SPRITES3.ASM"       ;31903
        INCLUDE "FONTPANT2.ASM"      ;32671

	ORG 32768
;-----------------------------------------------------------------
;    Valores iniciales del inicio de la rutina de imp_fondos
        ld hl,b_impfondo+18       ;activado el fondo al inicio
        ld a,l
        ld (VIMPFON),A            ;el valor cambia en la partida -
        ld a,h
        ld (VIMPFON+1),A          ;y se mantiene para la siguiente
;-----------------------------------------------------------------
;reseteo de valores, limpieza de buffers, impr.menu, borrado, etc
INI_CIO
        XOR A
        OUT (254),A
        call LIMPIOBUFFER  ; (lo usamos para desenrollar la m�sica)
	LD (CONTROL),A     ; Vaciar la variable de control
        ld (FRAMEV),A
	LD (FRAME),A
	LD (FLAGNOT),A
	LD (FLAGFIN),A
        LD (CANCION),A     ; MUSICA DEL MENU = 0
        ld a,129
	LD (FINAL),A       ;variable de cambio de fase y final de juego
        CALL RUBORCO	   ;Borrar pantalla
;-----------------------------------------------
;      Impresion de menu y musica a to'trapo
        CALL IMPRIMENU	   ;imprimir men
        call rest_notas
	call RESTPUNPINOT  ;PINTARNOTA.ASM restauro el puntero al inicio
	call RUTIMNOT      ;Pongo a 1 las notas a recoger (DATGAME.ASM)
        call ponmusicaint  ;cargo puntero rutina de interr.
        call PLAYER_wyz    ;Player de wyz
        DI
        LD A,#9C           ;Comienzo tabla interrup.
        LD I,A
        IM 2
        EI
        call CONTROLES
        call PLAYER_OFF
        call cleartabene   ;Vacio la tabene
        CALL RUBORCO	   ;Borrar pantalla
;Fin de inicializacion de valores y reseto

;---------------------------------------------------------------------------
;COMIENZA EL JUEGO
        call EMPEZAMOS    ;Imprimo el marcador
	LD A,1
        LD (DIRDMOV),A
        INC A
        LD (SENPRO),A     ;SENPRO
        LD (ANCHO),A      ;ANCHO 2
        LD (ALTO),A
        LD (COLU),A       ;COLU
        ld (LAST_P),A
        add a,2
	LD (VIDAS),A      ;vidas 3
        ld a,10

        LD (FILA),A      ;FILA
        LD (LAST_P+1),A
        ld a,8
        LD (PANTA),A      ;PANTA             DEJAR PARA EL JUEGO
        LD (LAST_SE),A	  ;
        dec a            ;valor objetos a coger en el juego = 7
        LD (OBJE),A
        LD A,16
        LD (ALTOSC),A     ;ALTO EN SCANNES DEL PROTA
INIVARSPR:
;---------------------------------------------------------------------------
;INICIALIZACION DE VARIABLES DE LA POSIC.DEL SPRITE
        LD DE,PROTADE
        LD A,E
        LD (PRODE),A
        LD A,D
        LD (PRODE+1),A
        LD DE,PROTAIZ                              ;antes prota 7
        LD A,E
        LD (PROIZ),A
        LD A,D
        LD (PROIZ+1),A
        LD DE,PROTARR
        LD A,E
        LD (PROAR),A
        LD A,D
        LD (PROAR+1),A
        LD DE,PROTAAB                              ;antes prota 7
        LD A,E
        LD (PROAB),A
        LD A,D
        LD (PROAB+1),A
CONBINIV:
;---------------------------------------------------------------------------
        call LLAMADA             ;;VACIADO DE PANTALLA INTERMEDIA
;---------------------------------------------------------------------------
;  CONTROL de juego

GAME
        ld hl,intgame   ;puntero de rutina de interrupciones
        ld (intaddr+1),hl
TECLANT:
        LD A,(FINAL)
        CP 128
        jp C,FINALJUEGO     ;Si el valor es 0 finaljuego
        JR NZ,RUCON         ;Si flag de 0 a 1, pasas de fase
        ld a,1              ;Hacer sonar el player para la pantalla intermedia
        ld (CANCION),a      ;jingle de interludio SONG=1
        call ponmusicaint   ;cargo puntero rutina de interr.
        call PLAYER_wyz     ;Player de wyz
        call pantinterm
        call PLAYER_OFF
        JR CONBINIV         ;Etiqueta para CALL LLAMADA
RUCON:
        LD A,(VCHOQE)
        CP 1
        JP Z,MUERTO
        LD A,(FLAGPA)
        CP 1
        JR NZ,CONT_
        LD A,(SENPRO)      ;LE METO EL VALOR DE SENPRO EN ESTA VARIABLE PARA QUE ME LA
        LD (LAST_SE),A     ;GUARDE Y RESTAURARLA CUANDO TE MATEN.
        JR CONBINIV         ;Etiqueta para CALL LLAMADA
CONT_
        ld hl,0
        ld (TIMER),HL
WAITNLOOP:
        call destello      ;parpadea si es el caso
        LD HL,(TIMER)
        LD A,H
        CP #FF
        JR NZ,WAITNLOOP
        CALL MIRARKEY
        ld a,(VARMOV)       ;Variable de movimiento del sprite
        CP 1
        JR Z,AGAME
        call NOMOVER        ;EN moversp.asm
AGAME
        halt
        call VOLCAR_ENE
        call DETCHOQ        ;DETCHOQUES.ASM
        XOR A
        LD (VARMOV),A
        JR GAME
MIRARKEY:
;---------------------------------------------------------------------------
;miramos si pulsamos teclas o usamos joystick
        LD HL,(CONTROL)    ;Valor que se nutre en RUCONTROL.ASM
        JP (HL)

;---------------------------------------------------------------------------
; RUTINA DE JOYSTICK
;---------------------------------------------------------------------------
JOY:
       LD E,0
       LD A,231          ;SINCLAIR JOYSTICK O LO QUE ES LO MISMO
       IN A,(#FE)        ;TECLAS DEL 6 AL 0
       PUSH AF
       XOR 31            ;ENMASCARAMOS LAS TECLAS DEL 6 AL 9
       RRA               ;DISCRIMINAMOS EL BIT 0
       RRA
       RL E              ;PASAMOS ARRIBA
       RRA
       RL E              ;PASAMOS ABAJO
       LD D,E            ;GUARDO LOS DATOS
       POP AF
       XOR 31
       RRA
       RRA
       RRA               ;DISCRIMINAMOS MOV.VERTICAL
       RRA
       RL E              ;METO MOV.DERECHA
       RRA
       RL E              ;METO MOV.IZQ
       SLA E
       SLA E             ;LAS DEJO EN SU SITIO
       LD A,D            ;CARGO D PARA SUMAR A E
       OR E
       LD E,A
       JR DECONT

;---------------------------------------------------------------------------
; RUTINA DE TECLADO
;---------------------------------------------------------------------------

MT
       LD E,0
       LD A,#DF
       IN A,(#FE)
       XOR 3            ;ENMASCARAMOS LAS TECLAS O y P
       RRA
       RL E
       RRA
       RL E
       LD A,#FB
       IN A,(#FE)
       XOR 1
       RRA
       RL E
       LD A,#FD
       IN A,(#FE)
       XOR 1
       RRA
       RL E             ;en E tenemos el valor de las pulsaciones de p,o,q,a
;---------------------------------------------------------------------------
; RUTINA DE EVENTOS DE CONTROL

DECONT
        LD A,E          ;comprobaciones de doble pulsacion de tecla
        AND 3           ;que har� que se diriga o no a un lugar concreto.
        CP 3
        JR NZ,CD1
        CALL ARRI       ;si pulsadas q y a entonces sube arriba
        JP AFINTEC
CD1     LD A,E
        AND 12
        CP 12           ;si pulsadas p y o entonces ve a la derecha
        JR NZ,CD2
        CALL DERE
        JP AFINTEC
CD2     LD A,E
        AND 15
        CP 5            ;si pulsadas o y a entonces ve a izquierda
        JR NZ,CD3
        CALL IZQ
        JP AFINTEC
CD3     LD A,E
        AND 15
        CP 6            ;si pulsadas o y q ve a izquierda
        JR NZ,CD4
        CALL IZQ
        JP AFINTEC
CD4     LD A,E
        AND 15
        CP 9            ;si pulsadas p y a ve a derecha
        JR NZ,CD5
        CALL DERE
        JP AFINTEC
CD5     LD A,E
        AND 15
        CP 10           ;si pulsadas p y q ve a derecha
        JR NZ,CD6
        CALL DERE
        JP AFINTEC
CD6                    ;fin comprobaciones de doble pulsacion de tecla
        AND A
        SRA E
        PUSH AF
        PUSH DE
        CALL C,ABA
        POP DE
        POP AF
        SRA E
        PUSH AF
        PUSH DE
        CALL C,ARRI
        POP DE
        POP AF
        SRA E
        PUSH AF
        PUSH DE
        CALL C,IZQ
        POP DE
        POP AF
        SRA E
        PUSH AF
        PUSH DE
        CALL C,DERE
        POP DE
        POP AF
AFINTEC
       ret
;---------------------------------------------------------------------------
; FIN RUTINA DE CONTROL

;---------------------------------------------------------------------------
; Actualizacion de situacion y pantallas
LLAMADA
        xor a
        LD (FLAGPA),A
        LD (FLAGTUN),A
        LD (FLAGNOT),A
        LD (PRENTU),A   ;BAJO FLAG DE TRANSICION ENTRE TUNELES. (DETCHOQ)
        LD A,2
        LD (ALTO),A
        call ruvodatabene ;volcado tabene -> tabsprpan (ruvodatspr.asm)
	CALL MAPPING
        CALL IMP_FONDO
	CALL SEARCH_SCR
        call TUNELADORA
        CALL NOTASMU
	CALL VOL_CARATT
	CALL VOL_CAR
        CALL IM_PIX
        CALL IMPATRIB
        CALL MOV
	CALL ruvudatspr  ;volcado tabsprpan  -> tabene (ruvodatspr.asm)
        RET

;---------------------------------------------------------------------------
; RUTINA DE MUERTE Y PASO DE PANTALLA
MUERTO
        XOR A
        LD (VCHOQE),A
        LD A,3        ;SONIDO MUERTE
        LD (REPE_FX),A
        LD A,175
        LD (LONG_FX),A
        CALL FX48K

;---------------------------------------------------------------------------
;Borrado de vidas en el marcador
               push hl
               push de
               LD DE,borra_jonhy
               ld a,(VIDAS)
               cp 2
               jr z,vidas2
               jr nc,vidas3
               ld hl,life3      ;en impmarcadores2.asm
               jr bvidasX
vidas2         ld hl,life2
               jr bvidasX
vidas3         cp 4
               jr z,vidas4
               ld hl,life1
               jr bvidasX
vidas4         ld hl,life0
bvidasX        CALL LLAMIMPRIM    ;en impmenu.asm
               pop de
               pop hl
;Fin borrado de vidas

;----------------------------------------------------------------------
; Comprobaci�n de vidas 0 y en caso contrario restauraci�n de valores
        LD A,(VIDAS)
        DEC A
        LD (VIDAS),A
        AND A
        JP Z,SONMUERTE
        LD A,(LAST_SE)
        LD (PANTA),A
        call rubo_game
        LD A,(LAST_P)
        LD (COLU),A
        LD A,(LAST_P+1)
        LD (FILA),A
        ld a,2
        ld (ANCHO),A
        LD A,16
        LD (ALTOSC),A
        xor a
        ld (FRAMEV),A
	LD (FRAME),A
        jp INIVARSPR
SONMUERTE:
        call RUBORCO
        call NOLOGUAI
        call imprim_gameover
        call LOGUAI
        call NOLOGUAI
        xor a
        OUT (254),a
        call cambio_attr_pant    ;en ruborpaesp.asm
        call rutina_borrado_esp  ;SOLO PARA PROBAR  ruborpaesp.asm
        JP INI_CIO
;Fin de rutinas de MUERTE Y GAME OVER

;-------------------------------------------------------------------------------
; FX Sonidos
FX48K:  push bc
        ld a,(REPE_FX)
        LD B,A
ARMU    PUSH BC
        ld a,(LONG_FX)
	LD C,A
	XOR A
SONMU   OUT (254),A
        LD B,C
ESPERA  DJNZ ESPERA
        XOR 16
        DEC C
        JR NZ,SONMU
        POP BC
        DJNZ ARMU
        pop bc
        ret
;-------------------------------------------------------------------------------
;para aprovechar bytes
ponmusicaint:
        ld hl,intmusic
        ld (intaddr+1),hl
        ret
;---------------------------------------------------------------------------
FINALJUEGO:
;musica del final
        ld a,2              ;Hacer sonar el player para la pantalla intermedia
        ld (CANCION),a      ;jingle de interludio SONG=1
        call ponmusicaint
        call PLAYER_wyz     ;Player de wyz
        call RUBORCO
        call IMPRIM_FIN_JUEGO
        call LOGUAI            ;Espera pulsacion tecla para volver a jugar
        call NOLOGUAI          ;Esperas que levante el dedo el jugador.
        call PLAYER_OFF
;fin sonido musica
        call rutina_borrado_esp
        JP INI_CIO
;-----------------------------------------------------------------------
; rutina espera a que haya polsacion de teclas,
LOGUAI
        XOR A
        IN A,(#FE)
        CPL
        AND #1F
        JR Z,LOGUAI
        ret
;-----------------------------------------------------------------------
; Esta rutina espera a que no haya ninguna tecla pulsada,
NOLOGUAI:
        XOR A
        IN A, ($FE)
        OR 224
        INC A
        JR NZ, NOLOGUAI
        RET

;reservadas 60030 y 60031 para la direccion en el arch.pantalla del sprite.
VIDAS	EQU 60032 ;60100
OBJE	EQU 60033 ;60101
FILA	EQU 60034 ;60102
COLU	EQU 60035 ;60103
ANCHO	EQU 60036 ;60104
ALTO	EQU 60037 ;60105
VARMOV  EQU 60038 ;60106 Flag de sprite en movimiento.
PRODE	EQU 60039 ;60107
PROIZ	EQU 60041 ;60109
PROAR	EQU 60043 ;60111
PROAB	EQU 60045 ;60113
TICKS	EQU 60047 ;60115 Para las interrupciones
FRAME 	EQU 60048 ;116
SENPRO	EQU 60049 ;117
LAST_SE	EQU 60050 ;118
DEAD	EQU 60051 ;119
PANTA	EQU 60052 ;120
LAST_P	EQU 60053 ;121
AL_TILE	EQU 60055 ;123
AN_TILE EQU 60056 ;124
FIL_DAT	EQU 60057 ;125
COL_DAT	EQU 60058 ;126
POTI_IM EQU 60059 ;127
POTI_IA EQU 60061 ;129
FINAL   EQU 60063 ;131
VE_TILE EQU 60064 ;132
DIRDMOV EQU 60066 ;134
FLAGPA  EQU 60067 ;135
POSPRO  EQU 60068 ;136  y 60137
ALTOSC  EQU 60070 ;138 VARIABLE Q TIENE LA ALTURA DEL PROTA EN SCANNES (IMPROTA.ASM)
FRAMEV  EQU 60071 ;139
CONTROL	EQU 60072 ;140 Y 60141
FLAGTUN EQU 60074 ;142
COMPTUN EQU 60075 ;143
FLAGNOT EQU 60077 ;145
COMPNOT EQU 60078 ;146 y 60147
PPTABNO EQU 60080 ;148 Y 60149 POSC. PUNTERO DE NOTA A PINTAR EN MARCADOR
TIMER   EQU 60082 ;152 para las interrupciones
ANCHOSP EQU 60083 ;153
ALTOSP  EQU 60084 ;154
CRONOS  EQU 60085 ;155
VCHOQE  EQU 60086 ;156
PRODER	EQU 60087 ;157 VARIABLES PARA RECUPERAR EL FRAME
PROIZR	EQU 60089 ;159             ;DEL PROTA
PROARR	EQU 60091 ;161             ;IMPRESO EN PANTALLA
PROABR	EQU 60093 ;163             ;PARA CUANDO NO SE MUEVE
SENDISP EQU 60095 ;165             ;SENTIDO DEL DISPARO
TSPRPAN EQU 60096 ;166             ;2 BYTES GUARDAN LA POSIC.EN TABSPRPAN(Enem.en pan)
ENE_EP  EQU 60098 ;168             ;HAY ENEMIGOS EN PANTALLA?
VIMPFON EQU 60099 ;170             ;tienes que imprimir el fondo? 2bytes
VFONDO  EQU 60101 ;172             ;Has pulsado con o sin fondo? tecla 3 del menu
VIX4    EQU 60102 ;173             ;Guardo el valor ix+4 ix+5 para IA de ebem.mov.vert.en horiz.
FLAGFIN EQU 60104 ;175
CANCION EQU 60105 ;176
VPARPA  EQU 60106 ;177             ; Y 60178
REPE_FX EQU 60108 ;179
LONG_FX EQU 60109 ;180
D_PARPA EQU 60110             ;DELAY DE PARPADEO
COLOR   EQU 60111
PRENTU  EQU 60112
W_PAPER EQU 60113
;                                  8 bytes libres hasta 60118
        INCLUDE "MOVIMIENTOS.ASM"      ;33313
        INCLUDE "DIRSENPRO.ASM"      ;34567
        INCLUDE "IMPIX.ASM"           ;34698
        INCLUDE "CALCDIRBUFF.ASM"
        INCLUDE "MOVERSP.ASM"
        INCLUDE "VOLCAMIENTO.ASM"
        INCLUDE "IMPROTA.ASM"
        INCLUDE "IMPFONDO2.ASM"
        INCLUDE "BUPAIM.ASM"
        INCLUDE "MAPEADOR.ASM"
        INCLUDE "VOLCAR.ASM"
        INCLUDE "VOLCARATT.ASM"       ;
        INCLUDE "INYECTILE.ASM"       ;
        INCLUDE "INYECTILEATT.ASM"    ;
        INCLUDE "C_TILEDIRBUFF.ASM"   ;
        INCLUDE "RUTUNOTASMU.ASM"      ;36396
        INCLUDE "COMPTUNOTAS.ASM"     ;36527
        INCLUDE "PROTUNEL.ASM"
        INCLUDE "PRONOTA.ASM"         ;finaliza 37090
        include "GESTORENEM.ASM"
        INCLUDE "IMPENE.ASM"          ;37984 mirar  mas abajo
        INCLUDE "VOLCARATTSP.ASM"
        INCLUDE "VOLCARFONDO.ASM"
        INCLUDE "DETCHOQUES.ASM"       ;38302
        INCLUDE "DATAS_TILE2.ASM"       ;fin en 39834
                                        ;0 bytes libres
        INCLUDE "RUTINAINTERR.ASM"      ;39835
        INCLUDE "REST_NOTAS.ASM"        ;fin en 39898
                                        ;38 bytes libres
        INCLUDE "TABINTERR.ASM"         ;39936
        include "WYZPROPLAY37AZX.ASM"   ;40193
        ;compresor incluido      ;finaliza junto buffer musica en 42856 a 42860
                                        ;libres 340bytes
        include "GAMEOVER.ASM"          ;43200
        INCLUDE "SPRITES2b.ASM"         ;43520  fijo
        INCLUDE "CALCDIRMEMTAB4.ASM"    ;45056 fijo
                                        ;fin 45099 a 45390 = 212bytes
                                        ;de 45336 a 45390 = 54 bytes
;Desde la 45391 a la 49999 se encuentra el buffer de pantalla y atributos.
        INCLUDE "DATASPANT.ASM"         ;50000
        INCLUDE "SPRITESb.ASM"          ;56344
        INCLUDE "DATGAME.ASM"           ;59800
                                        ;13 BYTES LIBRES
        INCLUDE "TABPANTAS.ASM"         ;59900
        INCLUDE "RUBORPAESP.ASM"        ;60118
        INCLUDE "PROTA5.ASM"            ;60200
        INCLUDE "TABSPRITES.ASM"        ;61992 ya no cabe nada m�s.
        INCLUDE "RUVODATSP.ASM"
        INCLUDE "RUBORPA.ASM"
        INCLUDE "RUCONTROLES2.ASM"        ;64751 fin 64897
                                        ;libres 638bytes
