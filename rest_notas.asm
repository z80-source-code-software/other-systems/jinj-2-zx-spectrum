;Recolocar en DATAS_PANT las notas  a recoger en el juego
rest_notas:
         ld hl,TABNOTRE           ;En pintarnota.asm
         ld b,7
recolocanota:
         inc hl
         ld a,(hl)
         ld e,a
         inc hl
         ld a,(hl)
         ld d,a     ;DE tiene el valor de la posicion en DATAS_PANT
         ld a,15    ;Es el valor del tile NOTA MUSICAL en DATAS_TILE
         ld (de),a
         inc hl
         djnz recolocanota
         ret
