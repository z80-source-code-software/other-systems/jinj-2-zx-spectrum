; Rutina de Gesti�n de Enemigos
;#######################################################################
VOLCAR_ENE:                                                  ;37075
           PUSH HL
           PUSH DE
           LD IX,TABENE
BAJOVOL    PUSH IX
           LD A,(IX+0)
           CP #FF
           JR Z,FINGESTO       ;Si Z es que ya no hay mas enemigos
           cp 128              ;enemigo "vacio"
           jr Z,ESTATICO
           LD A,(IX+2)         ;Un enemigo normal puede estar estatico
           AND A               ;es el caso de un disparo que "termina"
           JR Z,VERMOVENE      ;Es un enemigo estatico y solo se imprime
           LD A,(IX+1)
           CP 6                ;codigo del disparo
           JP Z,DISPAROSP      ;Mueve el disparo
           LD A,(IX+3)
           AND A
           CALL Z,IMPATRIB_ENE
           CP 4
           JP Z,FRAMESPR_0     ;Si Z poner framesp a 0
           LD A,(IX+7)
           CP 1
           JR Z,SPR_hor
           JR C,SPR_ver
           CP 3
           JR Z,SPR_hor         ;si no va pabajo
SPR_ver    CALL VOL_FON_ENE_VER ;volcamos el fondo del enemigo en vert.
           CALL IMP_ENE         ;No necesitamos imp.ATTR del fondo
           JR A_ESTATICO
SPR_hor    CALL VOL_FON_ENE_HOR ;"     "              "        en hori.
           CALL IMP_ENE         ;"     "              "        "
A_ESTATICO LD A,(IX+3)
           INC A
           LD (IX+3),A         ;si no incrementa framesp
ESTATICO   POP IX
           LD DE,18
           ADD IX,DE
           JR BAJOVOL
FINGESTO   POP IX
           POP DE
           POP HL
           RET
VERMOVENE  LD A,(IX+1)
           CP 6       ;codigo del disparo
           CALL Z,ANULARDISPARO
           JR ESTATICO
;///////////////////////////////////////////////////////////////////////
;Si el enemigo es un disparo y est� est�tico es debido a :
;- fin del recorrido o choca c/obstaculo.Borrar y activar el enemigo.
;___________ Del disparo y otras hierbas _______________________________
ANULARDISPARO:
           CALL VOL_ATTR_F_ENE_TOTAL      ;En volcarattsp.asm
           CALL VOL_FON_ENE_HOR           ;Aunque el disparo sea 2x2
           LD A,128
           LD (IX+0),A   ;Disparo anulado
;Siguiente db's
           PUSH IX                        ;Reactivar el enemigo
           PUSH DE
           LD IX,TABENE
BSUB_MO    LD A,(IX+2)
           AND A
           JR Z,ESTAPARADO
           LD DE,18
           ADD IX,DE
           JR BSUB_MO
ESTAPARADO LD A,1
           LD (IX+2),A
           POP DE
           POP IX
           RET
;----------------------------------------------------------------------
DISPAROSP
           ld a,(ix+3)
           PUSH AF
           and a
           call z,IMPATRIB_ENE
           POP AF
           CP 2
           JP NC,FRAMESPR_0
           CALL VOL_FON_ENE_HOR
           CALL IMP_ENE
           JP A_ESTATICO
;_ Fin del disparo _____________________________________________________
;
;///////////////////////////////////////////////////////////////////////
;____ n� de frame = 0 __________________________________________________
FRAMESPR_0:
           xor A               ;Frame=0 y vete al disparo
           LD (IX+3),A
	   LD A,(IX+6)
	   cp 1
	   JP Z,IALOMEVA   ;__enemigo normal o ia (fxialomeva.asm)SIN ACABAR
; Comprobar obstaculos. No lo hay--> continuar,
; si lo hay entonces-->  ver tipene y actuar en consecuencia
;----Comprobaci�n de obst�culos ----------------------------------------
COMPOBSTACULOS:
           LD A,(IX+7)
           CP 1
           jp Z,VADERSPR
           jp C,VAARSPR
           CP 3
           JP Z,VAIZQSPR
;mov.vertical hacia abajo
           LD A,(IX+9)     ;en caso contrario va a abajo Mov.vertical
           cp 17     ;colu 27
           JP z,SP_NO_BAJ
           ADD A,3
           CALL COMP_OBST_V
           AND A
           JR Z,NO_OB_ABSP
           JP SP_NO_BAJ     ;Si hay obstaculo
NO_OB_ABSP CALL VOL_ATTR_F_ENE_VER  ;Vuelco el attr de la primera fila
           CALL VOL_FON_ENE_V_C      ;
           LD A,(IX+9)
           INC A
           LD (IX+9),A
           CALL NEWPOSENE
           CALL RUT_LOC_FRAME  ;calc.frame 0 del sprite y lo imprimo
           CALL C_DIRBUFF_SPRITE     ;Calculo la nueva pos.sprite en buff
           ld (ix+14),l
           ld (ix+15),h              ;Y la guardo
           call IMPATRIB_ENE
           JP ESTATICO
;mov.vertical hacia arriba
VAARSPR    LD A,(IX+9)
           cp 8     ;colu 27
           JP z,SP_NO_SUB
           DEC A
           CALL COMP_OBST_V
           AND A
           JR Z,NO_OB_ARSP
           JP SP_NO_SUB  ;Si hay obstaculo
NO_OB_ARSP ld a,(IX+9)
           push af         ;guardo posY
           add a,2
           ld (ix+9),a
           CALL VOL_ATTR_F_ENE_VER    ;Vuelco el attr de la ultima fila
           CALL C_DIRBUFF_SPRITE     ;Calculo la nueva pos.sprite en buff
           ld (ix+14),l
           ld (ix+15),h              ;Y la guardo
           CALL NEWPOSENE
           CALL VOL_FON_ENE_V_C        ;
           pop af   ;saco posY
           DEC A
           LD (IX+9),A
           CALL RUT_LOC_FRAME  ;calc.frame 0 del sprite y lo imprimo
           CALL C_DIRBUFF_SPRITE     ;Calculo la nueva pos.sprite en buff
           ld (ix+14),l
           ld (ix+15),h              ;Y la guardo
           CALL NEWPOSENE
           call IMPATRIB_ENE
           JP ESTATICO
;mov.horizontal hacia derecha
VADERSPR   ld a,2
           ld (alt_comp),a
           ld (des_regC),a
           LD A,(IX+8)
           cp 27     ;colu 27
           JP z,SP_NO_DER
           ADD A,3
           CALL COMP_OBST_H
           AND A
           JR Z,NO_OB_DESP
           JP SP_NO_DER   ;Si hay obstaculo
NO_OB_DESP CALL VOL_FON_ENE_H_C       ;
           CALL VOL_ATTR_F_ENE_HOR   ;Vuelco el attr de la primera colu
           LD A,(IX+8)
           INC A
           LD (IX+8),A
           CALL C_DIRBUFF_SPRITE     ;Calculo la nueva pos.sprite en buff
           ld (ix+14),l
           ld (ix+15),h              ;Y la guardo
           CALL RUT_LOC_FRAME  ;calc.frame 0 del sprite y lo imprimo
           CALL NEWPOSENE
           call IMPATRIB_ENE
           JP ESTATICO
;mov.horizontal hacia izquierda
VAIZQSPR   ld a,2
           ld (alt_comp),a
           ld (des_regC),a

           LD A,(IX+8)
           cp 2       ;colu 2
           jr z,SP_NO_IZQ
           DEC A
           CALL COMP_OBST_H
           AND A
           JR Z,NO_OB_IZSP
           JR SP_NO_IZQ   ;Si hay obstaculo
NO_OB_IZSP ld a,(IX+8)
           push af         ;guardo posY
           add a,2
           ld (ix+8),a
           CALL VOL_ATTR_F_ENE_HOR    ;Vuelco el attr de la ultima colu
           CALL C_DIRBUFF_SPRITE      ;Calculo la pos.de esa colu en buff
           ld (ix+14),l
           ld (ix+15),h               ;Y la guardo
           call NEWPOSENE             ;calculo la pos.en la mem.pantalla
           CALL VOL_FON_ENE_H_C       ;vuelco el fondo de la colu
           pop af   ;saco posX        ;devuelvo los valores iniciales
           DEC A                      ;y calculo lo mismo para el sprite.
           LD (IX+8),A
           CALL C_DIRBUFF_SPRITE      ;Calculo la nueva pos.sprite en buff
           ld (ix+14),l
           ld (ix+15),h               ;Y la guardo
           CALL NEWPOSENE             ;calculo la pos.en la mem.pantalla
           CALL RUT_LOC_FRAME  ;calc.frame 0 del sprite y lo imprimo
           JP ESTATICO
;//////////////////////////////////////////////////////////////////////
;__________ enemigos normales _________________________________________
SP_NO_IZQ  ld a,(ix+1)
           cp 6
           jr z,BSP_NO_IZQ    ;Anulo el disparo.
           LD A,1
           LD (IX+7),A   ;A derecha
           CALL SEARCH_TABSPR
           JP ESTATICO
BSP_NO_IZQ XOR A
           LD (IX+2),A
           JP ESTATICO
SP_NO_DER  ld a,(ix+1)
           cp 6
           jr z,BSP_NO_DER    ;Anulo el disparo.
           ld a,3
           LD (IX+7),A   ;A izquierda
           CALL SEARCH_TABSPR
           JP ESTATICO
BSP_NO_DER XOR A
           LD (IX+2),A
           JP ESTATICO
SP_NO_SUB  ld a,2
           LD (IX+7),A   ;A abajo
           CALL SEARCH_TABSPR
           JP ESTATICO
SP_NO_BAJ  XOR A
           LD (IX+7),A   ;A arriba
           CALL SEARCH_TABSPR
           JP ESTATICO
;//////////////////////////////////////////////////////////////////////
;___ subrutinas de comprobacion de obstaculos horizontal y vertical ___
;----- entrada X en A y salida en A 0 o 1       -----------------------
alt_comp defs 1     ;valor que utiliza esta rutina para indicar el alto del sprite.
des_regC defs 1     ;desplazamiento del registro C en la rutina.
COMP_OBST_H:
        ld c,a    ;el valor de la colux lo dejo en c
        LD A,(IX+9)  ;POSY
        LD B,A
        CALL COMP_ATSP
        ld a,(alt_comp)
        ld b,a
        ld a,(des_regC)
        ld c,a
lcomp   LD A,(HL)
        CP 1            ;Si color del fondo=1 entonces no hay obstaculo.
        JR Z,b_lcomp
        CP 7            ;COLOR DEL PROTA
        JR Z,b_lcomp
        djnz lcomp     ;hay obst�culo por lo que no desplazo C
        jr b_b_lcomp
b_lcomp
        ld de,#20
        add hl,de
        srl c
        djnz lcomp
b_b_lcomp
        ld a,c
        ret
;----- entrada X en A y salida en A 0 o 1       -----------------------
COMP_OBST_V:
        LD B,A      ;Valor modificado de ix+9
        LD A,(IX+8)
        ld c,a    ;el valor de la colux lo dejo en c
        CALL COMP_ATSP
        LD A,(HL)
        CP 7          ;COLOR DEL PROTA
        JR Z,BCOBSTV
        CP 1            ;Si color del fondo=1 entonces no hay obstaculo.
        JR NZ,CHOQBSV
BCOBSTV
        INC HL
        LD A,(HL)
        CP 7         ;COLOR DEL PROTA
        JR Z,N_CHSPV
        CP 1
        JR Z,N_CHSPV
CHOQBSV ld a,1          ;Hay obst�culo
        ret
N_CHSPV xor a           ;No hay obst�culo
        RET
;///////////////////////////////////////////////////////////////////////
;
; Rutina de comportamiento de enemigos - IA  lo mejor vale d'argo
;                                            Fx ialomeva
;///////////////////////////////////////////////////////////////////////
IALOMEVA       ld a,(ix+1)   ;codene
               cp 3
               jp nc,iavertical  ;si no se mueve horizontal.
;/////////////////// IA HORIZONTAL //////////////////////////////
;la ia horizontal consiste en disparar si el enemigo est� a tiro.
;Pero al disparar el enemigo se queda est�tico,
;----------------------------------------------------------------
                ld a,(ix+9)  ;posY
                ld b,a
                ld a,(FILA)
                cp b         ;si fila+1=posY
                jr z,comcodisant
		inc a
		jr z,comcodisant
                jp COMPOBSTACULOS ;Retorno para continuar donde estaba
comcodisant
;aqui tengo que ver que sentido lleva el enemigo
                ld a,(ix+7)
                cp 1
                jr z,comcodisantder
;el enemigo va a izquiedrdas
                ld a,(ix+8)        ;posX
                ld b,a
                ld a,(COLU)
                cp b
                jr c,disparacabronizq
                jp COMPOBSTACULOS ;Retorno para continuar donde estaba
comcodisantder
;el enemigo va a derechas
                ld a,(COLU)        ;posX
                ld b,a
                ld a,(ix+8)
                cp b
                jr c,disparacabronder
                jp COMPOBSTACULOS ;Retorno para continuar donde estaba
disparacabronizq
                ld a,3
                ld (SENDISP),A
;jr enchufa_disparo
                ld a,(ix+8)   ;Estamos cerca de un obstaculo?
                dec a
                dec a
                call COMP_OBST_H
                and a
                jr z,enchufa_disparo
                jp COMPOBSTACULOS
disparacabronder
                ld a,1
                ld (SENDISP),A
                ld a,(ix+8)
                add a,4
                call COMP_OBST_H
                and a
                jr z,enchufa_disparo
                jp COMPOBSTACULOS
enchufa_disparo            ;Creamos el churro db's donde poner disparo.
                xor a
                ld (ix+2),a        ;movene =0
                push ix
                ld a,(ix+8)       ;posX
                ld b,a
                ld a,(ix+9)       ;posY
                ld c,a
                ld ix,TABENE+54
                ld a,(PANTA)
                ld (ix+0),a
                ld a,6
                ld (ix+1),a
                ld a,1
                ld (ix+2),a
                ld a,(SENDISP)      ;sentido del disparo
                ld (ix+7),a
                cp 3                ;segun este valor actuamos
                jr nz,dis_SAL_der
                ld a,#80           ;El disparo va a la izquierda.
                ld (ix+4),a
                ld (ix+10),a
                ld a,#ae
                ld (ix+5),a
                ld (ix+11),a
                ld a,b      ;posX del enemigo
                sub 3
ret_dis_SAL     ld (ix+8),a
                ld a,c      ;posY del enemigo
                ld (ix+9),a
;siguientes son initframe,scr,buffer
                call NEWPOSENE
                call C_DIRBUFF_SPRITE
                ld (ix+14),l
                ld (ix+15),h
                pop ix
                jp COMPOBSTACULOS ;Retorno para continuar donde estaba
dis_SAL_der     ld a,#40          ;El disparo va la derecha
                ld (ix+4),a
                ld (ix+10),a
                ld a,#af
                ld (ix+5),a
                ld (ix+11),a
                ld a,b
                add a,3
                jr ret_dis_SAL
;/////////////////// IA ENEMIGOS //////////////////////////////
;          Enemigos volanteros que te perseguiran
;
;----------------------------------------------------------------
iavertical      cp 6
                jp z,A_ESTATICO    ;si es un disparo sigue.
                ld a,(ix+8)        ;Ver si esta en la vertical
                ld b,a             ;colux
                add a,1
                ld c,a
                ld a,(COLU)
                cp c
                jp z,detect_presen_v
                add a,1
                cp b
                jp z,detect_presen_v
                ld a,(ix+9)        ;si no esta en la vert.comprobar la horiz.
                ld b,a
                add a,1
                ld c,a
                ld a,(FILA)
                cp c
                jr z,detect_presen_h
                add a,1
                cp b
                jr z,detect_presen_h
                jp COMPOBSTACULOS ;Retorno para continuar donde estaba
detect_presen_h
;si la detecci�n en vertical es afirmativa, el movimiento del enemigo
;lo haremos distinto al detect_horizontal. Primero imprimiremos en el
;mismo lugar el cambio de sentido.
;mov.horizontal hacia izquierda
                ld a,3
                ld (alt_comp),a
                inc a
                ld (des_regC),a

                ld a,(ix+8)    ;colux_sp
                ld b,a
                ld a,(COLU)
                cp b
                jp c,pro_a_izq_enem
;si no est� a la derecha

                ld a,b
                cp 27     ;colu 27
                jp z,SP_IA_DER
                ld a,b
                add a,2
                call COMP_OBST_H
                and a
                jr z,IA_NO_OB_DESP
                jp SP_IA_DER   ;Si hay obstaculo
IA_NO_OB_DESP
;Imprimir aqui el movimiento a derecha del IA
                ld a,(ix+4)
                ld (VIX4),a
                ld a,(ix+5)
                ld (VIX4+1),a
                ld a,(ix+1)
                cp 3
                jr z,gorgorolade
                cp 4
                jr nz,fin_imp_ia_esp_ene
                ld a,#df
                ld (ix+4),a
                ld (ix+10),a
                ld a,#7e                ;ghost fram.1 a der #7edf
                ld (ix+5),a
                ld (ix+11),a
                call VOL_FON_ENE_VER
                call IMP_ENE
                call delayIA
                ld a,#3f                ;ghost fram.2 a der #7f3f
                ld (ix+4),a
                ld (ix+10),a
                ld a,#7f
                ld (ix+5),a
                ld (ix+11),a
                call VOL_FON_ENE_VER
                call IMP_ENE
                call delayIA
                jr afin_imp_ia_esp_ene

gorgorolade     ld a,#5F
                ld (ix+4),a
                ld (ix+10),a
                ld a,#7D                ;#7d5f
                ld (ix+5),a
                ld (ix+11),a
                call VOL_FON_ENE_VER
                call IMP_ENE
                call delayIA
                ld a,#bf                ;#7dbf
                ld (ix+4),a
                ld (ix+10),a
                ld a,#7d
                ld (ix+5),a
                ld (ix+11),a
                call VOL_FON_ENE_VER
                call IMP_ENE
                call delayIA
afin_imp_ia_esp_ene:
                ld a,(VIX4)
                ld (ix+4),a
                ld a,(VIX4+1)
                ld (ix+5),a
fin_imp_ia_esp_ene
;fin de impresion
                CALL VOL_ATTR_F_ENE_HOR_IA   ;Vuelco el attr de la primera colu
                CALL VOL_FON_ENE_H_IAC       ;
                LD A,(IX+8)
                INC A
                LD (IX+8),A
                CALL C_DIRBUFF_SPRITE     ;Calculo la nueva pos.sprite en buff
                ld (ix+14),l
                ld (ix+15),h              ;Y la guardo
                CALL RUT_LOC_FRAME  ;calc.frame 0 del sprite y lo imprimo
                CALL NEWPOSENE
                call IMPATRIB_ENE
                JP ESTATICO
delayIA
                ld bc,0
bdelayIA2       djnz bdelayIA2
                ret
pro_a_izq_enem
                ld a,b
                dec a
                call COMP_OBST_H
                and a
                jr z,IA_NO_OB_IZSP
                jp SP_IA_IZQ   ;Si hay obstaculo
IA_NO_OB_IZSP
;Imprimir aqui el movimiento a izq. del IA
                ld a,(ix+4)
                ld (VIX4),a
                ld a,(ix+5)
                ld (VIX4+1),a
                ld a,(ix+1)
                cp 3
                jr z,gorgorolaiz
                cp 4
                jr nz,fin_imp_ia_esp_ene_i
                ld a,#1f
                ld (ix+4),a
                ld (ix+10),a
                ld a,#7e                ;ghost fram.1 a der
                ld (ix+5),a
                ld (ix+11),a
                call VOL_FON_ENE_VER
                call IMP_ENE
                call delayIA
                ld a,#7f
                ld (ix+4),a
                ld (ix+10),a
                ld a,#7e
                ld (ix+5),a
                ld (ix+11),a
                call VOL_FON_ENE_VER
                call IMP_ENE
                call delayIA
                jr afin_imp_ia_esp_ene_i
gorgorolaiz     ld a,#9F
                ld (ix+4),a
                ld (ix+10),a
                ld a,#7c
                ld (ix+5),a
                ld (ix+11),a
                call VOL_FON_ENE_VER
                call IMP_ENE
                call delayIA
                ld a,#FF
                ld (ix+4),a
                ld (ix+10),a
                ld a,#7C
                ld (ix+5),a
                ld (ix+11),a
                call VOL_FON_ENE_VER
                call IMP_ENE
                call delayIA
afin_imp_ia_esp_ene_i:
                ld a,(VIX4)
                ld (ix+4),a
                ld a,(VIX4+1)
                ld (ix+5),a
;fin de impresion
fin_imp_ia_esp_ene_i:

                ld a,(IX+8)
                push af         ;guardo posY
                inc a
                ld (ix+8),a
                call C_DIRBUFF_SPRITE      ;Calculo la pos.de esa colu en buff
                ld (ix+14),l
                ld (ix+15),h               ;Y la guardo
                call NEWPOSENE             ;calculo la pos.en la mem.pantalla
                call VOL_FON_ENE_H_IAC       ;vuelco el fondo de la colu
                call VOL_ATTR_F_ENE_HOR_IA ;Vuelco el attr de la ultima colu
                pop af   ;saco posX        ;devuelvo los valores iniciales
                dec a                      ;y calculo lo mismo para el sprite.
                ld (IX+8),a
                call C_DIRBUFF_SPRITE      ;Calculo la nueva pos.sprite en buff
                ld (ix+14),l
                ld (ix+15),h               ;Y la guardo
                call NEWPOSENE             ;calculo la pos.en la mem.pantalla
                call RUT_LOC_FRAME  ;calc.frame 0 del sprite y lo imprimo
                jp ESTATICO
detect_presen_v                              ;37860
                ld a,(ix+9)  ;filasp
                ld b,a
                ld a,(FILA)
                cp b
                jr c,__pro_x_enc_ene
;el prota esta por debajo del enemigo
;miramos el sentido del enemgio. Subia o bajaba?
                ld a,(ix+7)      ;sentene
                cp 2
                jp z,COMPOBSTACULOS ;Retorno para continuar donde estaba
                                  ;si estaba subiendo continua
;En el caso de que bajara,debemos cambiar el sentido del enemigo
;como cuando encuentra un obstaculo.
ch_sen_sp_ar    ld a,2
                ld (ix+7),a   ;A abajo
                call SEARCH_TABSPR
                jp ESTATICO
__pro_x_enc_ene                            ;37890
;el prota esta por encima del enemigo
;miramos el sentido del enemgio. Subia o bajaba?
                ld a,(ix+7)      ;sentene
                and a
                jp z,COMPOBSTACULOS ;Retorno para continuar donde estaba
                                  ;si estaba bajando continua
;En el caso de que bajara,debemos cambiar el sentido del enemigo
;como cuando encuentra un obstaculo.
ch_sen_sp_ab    xor a
                ld (ix+7),a   ;A abajo
                call SEARCH_TABSPR
                jp ESTATICO
;_______________Cambio de rumbo de los enemigos________________________
;----------------------------------------------------------------------
;enemigos con IA volanteros los de der.e iaq. con IA solo disparan
SP_IA_IZQ  XOR A
           LD (IX+7),A   ;Venia de la izquierda y lo mando para arriba
;(Aqui calculo el frame e imprimo el nuevo en el sitio)
           call SEARCH_TABSPR
           JP ESTATICO
SP_IA_DER  LD A,2
           LD (IX+7),A   ;Ahora para abajo
;(Aqui calculo el frame e imprimo el nuevo en el sitio)
           call SEARCH_TABSPR
           JP ESTATICO
;///////////////////////////////////////////////////////////////////////
;______ Rutinas de uso general _________________________________________
;
;-----------------------------------------------------------------------
RUT_LOC_FRAME:
           LD A,(IX+1)
           CP 6
           JR NZ,B_RUT_LOC_FRAME   ;Si es el disparo restar 2frames
           LD DE,#C0
           JR BB_RUT_LOC_FRAME
B_RUT_LOC_FRAME
           LD DE,#180     ;pasamos de frame sumando 96. Un sprite=384
BB_RUT_LOC_FRAME
           LD A,(IX+4)
           LD L,A
           LD A,(IX+5)
           LD H,A
           AND A          ;Acarreo a 0
           SBC HL,DE      ;otros 96, por lo tanto restamos #1E0=480
           LD A,L
           LD (IX+4),A
           LD A,H
           LD (IX+5),A
           ret
;_____ Busqueda de Frame 0 en la tabla de enemigos ____________________
SEARCH_TABSPR:
           push af       ;Guardo el nuevo valot de sentene
           LD HL,TABSPR
           LD A,(IX+1)   ;Codene
           add a,a
           add a,a
           ld b,0
           ld c,a
           add hl,bc
           pop af        ;pa donde va a ir ahora ?
           CP 3
           JR Z,AIZSPFRAM
           AND A
           JR Z,AIZSPFRAM
;Los enemigos en vertical tienen solo 4 frames.Y si el enemigo va a ir
;ahora a la derecha ya estoy en la posicion de la tabla TABSPR
FIN_SEARC_TABSPR:
           LD A,(HL)
           LD (IX+4),A
           INC HL
           LD A,(HL)
           LD (IX+5),A
           RET
AIZSPFRAM  INC HL
           INC HL
           JR FIN_SEARC_TABSPR
;___________ Calculo de la nueva posicion del enemigo _______________
NEWPOSENE:
          LD A,(IX+9)   ;Y
          LD L,A
          LD A,(IX+8)   ;X
          CALL tabla4metal
          LD A,L
          LD (IX+12),A
          LD A,H
          LD (IX+13),A
          RET
;_______________________________________________________________________
;RUTINA DE COORD.ATRIB.SPRITES. B=FILA C=COLU CARACT.(coordficu) IMPIX.ASM
COMP_ATSP:
                ld l,c  ;1/4esta linea es necesaria para este juego concretamente.
                ld h,#58    ;2/7
                xor a       ;1/4
                srl b       ;2/8
                rra         ;1/4
                srl b       ;2/8
                rra         ;1/4
                srl b       ;2/8
                rra         ;1/4
                ld c,a      ;1/4
                add hl,bc   ;3/11
                ret         ;3/10      ;ciclos= 20 / 76t-states