;RUTINA DE DETECCION DE CHOQUES
;#####################################
DETCHOQ:
                ld a,(PRENTU)         ;Compruebo que el prota no esta en
                cp 1                  ;transicion entre tuneles.
                ret z
                PUSH DE
                PUSH HL
                call calc_pxy_pro
                LD IX,TABENE
Bajodet         PUSH de
                push ix
                LD A,(IX+0)
                cp 128             ;si NO enemigo, salta.
                jr z,b_Bajodet
                jr nc,Findet       ;Si 255 finaliza
                CALL DETECTAR
b_Bajodet       pop ix
                ld de,18
                add ix,de
                pop de
                jr Bajodet
Findet          pop de
                pop ix
                POP HL
                POP DE
                RET
DETECTAR
;QUE TIPO DE MOVIMIENTO LLEVA EL ENEMIGO ?
        LD A,(IX+7)	;ENEMIGO VERT U HORIZ?
        AND A
        JR Z,DETV
        CP 2
        JR Z,DETV
DETH:			;ENEM.EN MOV.HORIZONTAL
        LD A,(IX+8)
        SLA A
        SLA A
        SLA A
        LD B,A
        LD A,(IX+3)
        ADD A,B
        LD B,A          ;COLUSP
        ADD A,14
        LD C,A          ;COLUSP +ANCHO
        LD A,C          ;comprobacion eje horizontal
        CP H
        RET C
        LD A,B
        CP L
        RET NC
        LD A,(IX+9)
        SLA A
        SLA A
        SLA A
        LD B,A          ;filaSP
        ADD A,14
        LD C,A          ;filaSP +alto
        LD A,C          ;comprobacion eje vertical
        CP D
        RET C
        LD A,B
        CP E
        RET NC
        JR CHOQEUP

DETV:
        LD A,(IX+8)
        SLA A
        SLA A
        SLA A
        LD B,A
        ADD A,14
        LD C,A          ;COLUSP +ANCHO
        LD A,C          ;comprobacion eje horizontal
        CP H
        RET C
        LD A,B
        CP L
        RET NC
        LD A,(IX+9)	;TRASPASO FILA_SP A SCAN
        SLA A
        SLA A
        SLA A
        LD B,A		;
        LD A,(IX+3)     ;FRAMES SP
        ADD A,B
        LD B,A          ;GUARDO EN B = FILASP
        ADD A,14
        LD C,A          ;GUARDO EN C = FILASP+ALTO
        LD A,C          ;comprobacion eje vertical
        CP D
        RET C
        LD A,B
        CP E
        RET NC

CHOQEUP LD A,1
        LD (VCHOQE),A
        RET
calc_pxy_pro:
        LD A,(FILA)	;TRASPASO FILA A SCAN
        SLA A
        SLA A
        SLA A
        LD D,A          ;
        LD A,(FRAMEV)
        ADD A,D
        LD D,A          ;GUARDO EN D = FILA
        ADD A,14
        LD E,A          ;GUARDO EN E = FILA+ALTO
        LD A,(COLU)
        SLA A
        SLA A
        SLA A
        LD H,A
        LD A,(FRAME)
        ADD A,H
        LD H,A          ;GUARDO EN H = COLU
        ADD A,14
        LD L,A          ;GUARDO EN L = COLU +ANCHO
; DE Y HL TIENEN LOS LIMITES DEL PROTA
        ret

;#######################################
;FIN DETECCION CHOQUES