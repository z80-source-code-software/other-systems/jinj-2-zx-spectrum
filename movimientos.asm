;#####################################
; RUTINA DE DIRECCIONES DE MOVIMIENTO
;#####################################
ABA:
;---------------------------------------------------------------------------
;Al pulsar la A comprobar si DIRDMOV es la misma y retornar
               ld a,1
               ld (VARMOV),A ;Flag de movimiento del prota levantado
               LD A,(DIRDMOV)
               CP 3          ;Si vienes de pulsar la a es que dirdmov es 2
               JP Z,MM_ABA   ;Por lo que no has cambiado de direccion.
               CP 2          ;si era 2 estabas pulsando q antes de venir
               JR Z,CHO_DIRA
               CALL REST_FONDOS
               LD A,(FRAME)
               AND A
               JR Z,CHO_ETIAIZDE
               LD A,(DIRDMOV)
               AND A
               JR Z,SENTIA_DE        ;pulsas p si no pulsa o
               LD A,(FRAME)
               cp 3
               JR C,ASENTIA_IZF01
               JR CHO_ETIAIZDE

SENTIA_DE      LD A,(FRAME)
               CP 3
               JR C,CHO_ETIAIZDE
ASENTIA_IZF01:                   ;Iba a izquierdas y con frames 1 o 2 -0
               ld a,(COLU)   ;Al ir a izquierdas pero estar en los primeros
               inc a         ;frames.Debes sumar 8 a la columna para pintar
               ld (COLU),a   ;ajustado al caracter de subida que se imprime
CHO_ETIAIZDE:
               xor a             ;Iba a izquierdas y con frames 3 o 4 -5
               LD (FRAMEV),A
               ld (FRAME),A      ;Para NO tenerlo en cuenta en la det.choques
               ld a,2
               LD (ANCHO),A
               LD (ALTO),A
               CALL IMPATRIB
               ld a,3
               LD (DIRDMOV),A
               JP BLOP_AB
CHO_DIRA:      CALL REST_FONDOS
               CALL BUSCOR_VER
               CALL IMPATRIB
               LD A,3
               LD (DIRDMOV),A
               LD A,(FRAMEV)
               AND A
               JR NZ,BAB_AQUI
               JR BLOP_AB
MM_ABA:
        	LD A,3
        	LD (DIRDMOV),A   ;BANDERIN DE MOVIMIENTO ABAJO LEVANTADO
        	LD A,(FRAMEV)
        	AND A
        	JR Z,compchoab
        	cp 4
        	jr nc,LOPONGOA0_AB
        	LD A,2
        	LD (ANCHO),A
        	inc A
        	LD (ALTO),A
AB_AQUI        	LD A,(FRAMEV)
	        INC A
        	LD (FRAMEV),A
BAB_AQUI        LD A,24
	        LD (ALTOSC),A
	        LD A,(PROAB)
        	LD E,A
        	LD (POSPRO),A
        	LD A,(PROAB+1)
        	LD D,A        ;TENGO EN DE LA POSIC.DEL FRAME NUEVO A IMPRIMIR
        	LD (POSPRO+1),A
        	CALL MOVER
        	RET
LOPONGOA0_AB:
        	CALL C_DIRBUFFSPATT
        	CALL VOLCARSPATVE      ;Vuelco el attr del fondo del sprite
        	LD A,2               ;Imprimo solo el fondo columna atr�s d.sprite
        	LD (ANCHO),A
        	XOR A
        	LD (FRAMEV),A
        	CALL Avolcamiento   ;VUELCO EL FONDO DEL SPRITE
        	LD A,(FILA)
        	INC A
        	LD (FILA),A
BLOP_AB        	ld a,2
        	LD (ALTO),A
        	LD (ANCHO),A
        	LD A,16
        	LD (ALTOSC),A
        	LD DE,PROTAAB
        	LD A,E
        	LD (PROAB),A
        	LD A,D
        	LD (PROAB+1),A
        	CALL MOVER
        	CALL comprob_tunotas
        	RET
compchoab
        	LD A,(FILA)
        	CP 20                ;MAX.POSIC.PANTALLA ABAJO
        	JR NZ,YSIAB
		LD A,2
        	LD (SENPRO),A
        	ld (ANCHO),A
        	ld (ALTO),A
        	dec a
        	LD (FLAGPA),A    ;BANDERIN PARA SABER QUE VAMOS A PASAR DE PANTALLA
        	xor a
        	LD (FRAMEV),A
        	ld a,6
        	LD (FILA),A
        	LD (LAST_P+1),A  ;ESTAS LINEAS inicializan VALORES CUANDO MUERES
        	LD A,(COLU)
        	LD (LAST_P),A
        	LD A,16
        	LD (ALTOSC),A
        	LD DE,PROTAAB
        	LD A,E
        	LD (PROAB),A
        	LD A,D
        	LD (PROAB+1),A
        	RET
YSIAB:
        	ld a,(FILA)
        	add a,2
        	ld (FILA),a          ;vamos a comprobar si podemos seguir
        	CALL IMP_AT
        	push hl
        	LD A,(HL)       ;en impix.asm
        	CP 1            ;valor del fondo (colour crash) 1 azul 0 sin fondo
        	JR NZ,NOAB
        	inc l            ;ver siguiente columna
        	LD A,(HL)
        	CP 1
        	JR Z,SI_AB
NOAB    	pop hl
        	LD A,(FILA)
        	sub 2
        	LD (FILA),A
        	JP BLOP_AB
SI_AB   	pop hl
        	LD A,(FILA)
        	sub 2
        	LD (FILA),A
        	ld a,3
        	ld (ALTO),A
        	DEC A
        	LD (ANCHO),A
        	CALL IMPATRIB    ;Imprimo el attr del fondo del prota.
        	call sopasos
        	JP AB_AQUI
ARRI:
;---------------------------------------------------------------------------
               ld a,1
               ld (VARMOV),A ;Flag de movimiento del prota levantado

               LD A,(DIRDMOV)
               CP 2          ;Si vienes de pulsar la q es que dirdmov es 2
               JP Z,MM_ARR   ;Por lo que no has cambiado de direccion.
               CP 3          ;si era 3 estabas pulsando a antes de venir
               JR Z,CHO_DIRQ
               CALL REST_FONDOS
               LD A,(FRAME)
               AND A
               JR Z,CHO_ETIQIZDE
               LD A,(DIRDMOV)
               AND A                 ;pulsabas p
               JR Z,SENTIQ_DE
               LD A,(FRAME)
               cp 3
               JR C,ASENTIQ_IZF01
               JR CHO_ETIQIZDE

SENTIQ_DE      LD A,(FRAME)
               CP 3
               JR C,CHO_ETIQIZDE
ASENTIQ_IZF01:
               ld a,(COLU)   ;Ibas a izquierdas pero estas en los primeros
               inc a         ;frames. Debes sumar 8 a la colu para pintar lo
               ld (COLU),a   ;m�s justo al char de subida que vas a imprimir

CHO_ETIQIZDE   xor a
               ld (FRAMEV),A
               ld (FRAME),A      ;Para NO tenerlo en cuenta en la det.choques

               ld a,2
               LD (ANCHO),A
               LD (ALTO),A
               CALL IMPATRIB
               LD A,2
               LD (DIRDMOV),A
               JP BLOP_AR      ;En moviments.asm
CHO_DIRQ:      call REST_FONDOS
               CALL BUSCOR_VER
               CALL IMPATRIB
               LD A,2
               LD (DIRDMOV),A   ;BANDERIN DE MOVIMIENTO ARRIBA LEVANTADO
	       LD A,(FRAMEV)
	       AND A
	       JR NZ,BAR_AQUI
               jr BLOP_AR
MM_ARR:
        	LD A,2
        	LD (DIRDMOV),A   ;BANDERIN DE MOVIMIENTO ARRIBA LEVANTADO
        	LD A,(FRAMEV)
        	AND A
        	JR Z,compchoar
        	cp 4
        	jr nc,LOPONGOA0_AR
        	LD A,2
        	LD (ANCHO),A
        	inc A
        	LD (ALTO),A
AR_AQUI:      	LD A,(FRAMEV)
        	INC A
        	LD (FRAMEV),A
BAR_AQUI       	LD A,24
        	LD (ALTOSC),A
        	LD A,(PROAR)
        	LD E,A
        	LD (POSPRO),A
        	LD A,(PROAR+1)
        	LD D,A        ;TENGO EN DE LA POSIC.DEL FRAME NUEVO A IMPRIMIR
        	LD (POSPRO+1),A
        	CALL MOVER
        	RET
LOPONGOA0_AR:
        	ld a,(FILA)
        	PUSH AF
        	ADD A,2
        	LD (FILA),A
        	CALL C_DIRBUFFSPATT
        	CALL VOLCARSPATVE      ;Vuelco el attr del fondo del sprite
        	ld a,8
        	ld (ALTOSC),A
	        LD A,2               ;Imprimo solo el fondo columna atr�s d.sprite
        	LD (ANCHO),A
        	XOR A
        	LD (FRAMEV),A
        	CALL Avolcamiento   ;VUELCO EL FONDO DEL SPRITE
        	POP AF
        	LD (FILA),A
BLOP_AR        	ld a,2
        	LD (ALTO),A
        	LD A,16
        	LD (ALTOSC),A
        	LD DE,PROTARR
        	LD A,E
        	LD (PROAR),A
        	LD A,D
        	LD (PROAR+1),A
        	CALL MOVER
        	CALL comprob_tunotas
        	RET
compchoar
        	LD A,(FILA)
        	CP 6                ;MAX.POSIC.PANTALLA A DERECHA
        	JR NZ,YSIAR
		XOR A
        	LD (SENPRO),A
        	LD (FRAMEV),A
        	LD A,2
        	ld (ANCHO),A
        	ld (ALTO),A
        	dec a
        	LD (FLAGPA),A    ;BANDERIN PARA SABER QUE VAMOS A PASAR DE PANTALLA
        	ld a,20
        	LD (FILA),A
        	LD (LAST_P+1),A  ;ESTAS LINEAS inicializan VALORES CUANDO MUERES
        	LD A,(COLU)
        	LD (LAST_P),A
        	LD A,16
        	LD (ALTOSC),A
        	LD DE,PROTARR
        	LD A,E
        	LD (PROAR),A
        	LD A,D
        	LD (PROAR+1),A
        	RET
YSIAR:
        	ld a,(FILA)
        	dec a
        	ld (FILA),a          ;vamos a comprobar si podemos seguir
        	CALL IMP_AT
        	push hl
        	LD A,(HL)       ;en impix.asm
        	CP 1            ;valor del fondo (colour crash) 1 azul 0 sin fondo
        	JR NZ,NOAR
        	inc l            ;ver siguiente columna
        	LD A,(HL)
        	CP 1
        	JR Z,SI_AR
NOAR    	pop hl
        	LD A,(FILA)
        	inc a
        	LD (FILA),A
        	JP BLOP_AR
SI_AR   	pop hl
        	ld a,3
        	ld (ALTO),A
        	CALL IMPATRIB    ;Imprimo el attr del fondo del prota.
        	call sopasos
        	JP AR_AQUI
IZQ:
;---------------------------------------------------------------------------
;Al pulsar la O comprobar si DIRDMOV es la misma y retornar
               ld a,1
               ld (VARMOV),A ;Flag de movimiento del prota levantado

               LD A,(DIRDMOV)
               CP 1          ;Si vienes de pulsar la o es que dirdmov es 2
               JP Z,MM_IZQ   ;Por lo que no has cambiado de direccion.
               AND A
               JR Z,CHO_DIRP
               CALL REST_FONDOS
               LD A,(FRAMEV)
               AND A
               JR Z,CHO_ETIOARAB
               LD A,(DIRDMOV)
               CP 2                 ;pulsabas Q
               JR Z,SENTIO_AR
               LD A,(FRAMEV)
               CP 3
               JR C,CHO_ETIOARAB
               JR ASENTII_ARF01
SENTIO_AR:     LD A,(FRAMEV)
               CP 3                ;EL VALOR DE FRAMEV DESPUES DE PINTAR
               JR NC,CHO_ETIOARAB   ;EL PROTA 2X2 ES 1
ASENTII_ARF01: LD A,(FILA)
               inc a
               LD (FILA),A         ;Imprimes una fila mas abajo
CHO_ETIOARAB:  xor a
               ld (FRAME),A
               ld (FRAMEV),A      ;Para NO tenerlo en cuenta en la det.choques

               ld a,2
               LD (ANCHO),A
               LD (ALTO),A
               CALL IMPATRIB
               ld a,1
               ld (DIRDMOV),A
               JP BLOP_IZ
CHO_DIRP:      CALL REST_FONDOS
               CALL BUSCOR_HOR
               CALL IMPATRIB
               ld a,1
               ld (DIRDMOV),A
               ld a,(FRAME)
               AND A
               JP Z,BIZ_AQUI
               ld a,3
               ld (ANCHO),A
               jp BIZ_AQUI    ;En moviments.asm
MM_IZQ:
        	LD A,1
        	LD (DIRDMOV),A   ;BANDERIN DE MOVIMIENTO A IZQUIERDA LEVANTADO
        	LD A,(FRAME)
	        and a
        	JR Z,compchoi
        	cp 4
        	jr nc,LOPONGOA0_IZ
        	LD A,3
        	LD (ANCHO),A
        	DEC A
        	LD (ALTO),A
IZ_AQUI        	LD A,(FRAME)
        	INC A
        	LD (FRAME),A
BIZ_AQUI	LD A,(PROIZ)	;Llamada desde dirsenpro.asm
        	LD E,A
        	LD (POSPRO),A
        	LD A,(PROIZ+1)
        	LD D,A        ;TENGO EN DE LA POSIC.DEL FRAME NUEVO A IMPRIMIR
        	LD (POSPRO+1),A
        	CALL MOVER
        	RET
LOPONGOA0_IZ:
        	ld a,(COLU)
        	push af
        	add a,2
        	ld (COLU),a
        	CALL C_DIRBUFFSPATT
        	CALL VOLCARSPAT      ;Vuelco el attr del fondo del sprite
        	LD A,1               ;Imprimo solo el fondo columna atr�s d.sprite
        	LD (ANCHO),A
        	DEC A
        	LD (FRAME),A
        	CALL Avolcamiento   ;VUELCO EL FONDO DEL SPRITE
        	pop af
        	ld (COLU),A
BLOP_IZ        	ld a,2
        	LD (ANCHO),A
        	LD (ALTO),A
        	LD A,16
        	LD (ALTOSC),A
        	LD DE,PROTAIZ
        	LD A,E
        	LD (PROIZ),A
        	LD A,D
        	LD (PROIZ+1),A
        	CALL MOVER
        	CALL comprob_tunotas
        	RET
compchoi          ; COMPROBACION DE CHOQUES BASICA. Mirarlo al pixel
        	LD A,(COLU)
        	and a                ;MAX.POSIC.PANTALLA A DERECHA
        	JR NZ,YSIIZ
        	ld a,1
        	LD (FLAGPA),A
        	inc a
        	LD (ANCHO),A
        	inc a
        	LD (SENPRO),A
        	ld a,30
        	LD (COLU),A
        	LD (LAST_P),A
        	LD A,(FILA)
        	LD (LAST_P+1),A
        	LD A,16
        	LD (ALTOSC),A
        	LD DE,PROTAIZ
        	LD A,E
        	LD (PROIZ),A
        	LD A,D
        	LD (PROIZ+1),A
        	RET
YSIIZ:
        	LD A,(COLU)    ;Comprobando solo esa vez si hay o no tile contiguo
        	dec a
        	LD (COLU),A
        	CALL IMP_AT
        	push hl
        	LD A,(HL)       ;en impix.asm
        	CP 1            ;valor del fondo si lo hay es 1 si no es siempre 0
        	JR NZ,NOIZ
        	ld de,#20
        	add hl,de
        	LD A,(HL)
        	CP 1
        	JR Z,SI_IZ
NOIZ    	pop hl
        	LD A,(COLU)
        	inc a
        	LD (COLU),A
        	JP BLOP_IZ
SI_IZ   	pop hl
        	LD A,3
        	LD (ANCHO),A
        	DEC A
        	LD (ALTO),A
        	CALL IMPATRIB
        	call sopasos
        	JP IZ_AQUI
DERE:
;---------------------------------------------------------------------------
;Al pulsar P comprobar si DIRDMOV es la misma y retornar
               ld a,1
               ld (VARMOV),A ;Flag de movimiento del prota levantado

               LD A,(DIRDMOV)
               AND A         ;Si vienes de pulsar la p es que dirdmov es 2
               JP Z,MM_DER   ;Por lo que no has cambiado de direccion.
               CP 1
               JR Z,CHO_DIRO ;"o"
               CALL REST_FONDOS
               LD A,(FRAMEV)
               AND A
               JR Z,CHO_ETIPARAB
               LD A,(DIRDMOV)
               CP 2                 ;pulsabas Q
               JR Z,SENTIP_AR
               LD A,(FRAMEV)
               CP 3
               JR C,CHO_ETIPARAB
               JR ASENTIPAR
SENTIP_AR:     LD A,(FRAMEV)
               cp 3
               JR NC,CHO_ETIPARAB
ASENTIPAR      LD A,(FILA)
               inc a
               LD (FILA),A
CHO_ETIPARAB:  xor a
               LD (FRAME),A
               ld (FRAMEV),A      ;Para NO tenerlo en cuenta en la det.choques

               ld a,2
               LD (ANCHO),A
               LD (ALTO),A
               CALL IMPATRIB
               xor a
               ld (DIRDMOV),A
               JP BLOP_DE
CHO_DIRO       CALL REST_FONDOS
               CALL BUSCOR_HOR
               CALL IMPATRIB
               xor a
               ld (DIRDMOV),A
               ld a,(FRAME)
               AND A
               JP Z,BDE_AQUI
               ld a,3
               ld (ANCHO),A
               jp BDE_AQUI
MM_DER:
        	XOR A
        	LD (DIRDMOV),A   ;BANDERIN DE MOVIMIENTO A DERECHA LEVANTADO
        	LD A,(FRAME)
        	and a
        	JR Z,compchod
        	cp 4
        	jr nc,LOPONGOA0_DE
        	LD A,3
        	LD (ANCHO),A
        	DEC A
        	LD (ALTO),A
DE_AQUI        	LD A,(FRAME)
        	INC A
        	LD (FRAME),A
BDE_AQUI       	LD A,(PRODE)
	        LD E,A
        	LD (POSPRO),A
        	LD A,(PRODE+1)
        	LD D,A        ;TENGO EN DE LA POSIC.DEL FRAME NUEVO A IMPRIMIR
        	LD (POSPRO+1),A
        	CALL MOVER
        	RET
LOPONGOA0_DE:
        	CALL C_DIRBUFFSPATT
        	CALL VOLCARSPAT      ;Vuelco el attr del fondo del sprite
        	LD A,1               ;Imprimo solo el fondo columna atr�s d.sprite
        	LD (ANCHO),A
        	DEC A
        	LD (FRAME),A
        	CALL Avolcamiento   ;VUELCO EL FONDO DEL SPRITE
        	LD A,(COLU)
        	INC A
        	LD (COLU),A
BLOP_DE        	ld a,2
        	LD (ANCHO),A
        	LD (ALTO),A
        	LD A,16
        	LD (ALTOSC),A
        	LD DE,PROTADE
        	LD A,E
        	LD (PRODE),A
        	LD A,D
        	LD (PRODE+1),A
        	CALL MOVER
        	CALL comprob_tunotas
        	RET
compchod          ; COMPROBACION DE CHOQUES BASICA. Mirarlo al pixel
        	LD A,(COLU)
        	CP 30                ;MAX.POSIC.PANTALLA A DERECHA
        	JR NZ,YSIDE
        	LD A,1
        	LD (SENPRO),A
        	LD (FLAGPA),A
        	xor a
        	LD (COLU),A
        	LD (LAST_P),A
        	LD A,(FILA)
        	LD (LAST_P+1),A
        	LD A,16
        	LD (ALTOSC),A
        	LD DE,PROTADE
        	LD A,E
        	LD (PRODE),A
        	LD A,D
	        LD (PRODE+1),A
	        LD A,2
	        LD (ANCHO),A
	        RET
YSIDE:
	        LD A,(COLU)    ;Comprobando solo esa vez si hay o no tile contiguo
	        ADD A,2
	        LD (COLU),A
	        CALL IMP_AT
	        push hl
	        LD A,(HL)       ;en impix.asm
	        CP 1            ;valor del fondo si lo hay es 1 si no es siempre 0
	        JR NZ,NODE
	        ld de,#20
	        add hl,de
	        LD A,(HL)
	        CP 1
	        JR Z,SI_DE
NODE    	pop hl
        	LD A,(COLU)
        	SUB 2
        	LD (COLU),A
        	JP BLOP_DE
SI_DE   	pop hl
        	LD A,(COLU)
        	SUB 2
        	LD (COLU),A
        	LD A,3
        	LD (ANCHO),A
        	DEC A
        	LD (ALTO),A
        	CALL IMPATRIB
        	call sopasos
        	JP DE_AQUI
sopasos
;SONIDO pasos
                ld a,1
                LD (REPE_FX),A
                LD A,2
                LD (LONG_FX),A
                CALL FX48K
                ret