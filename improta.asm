;##################### IMPRESION DE SPRITE
;-------------------------------------------------------------------------
MOV:
        LD DE,(60030)
        LD A,(DIRDMOV)
        AND A
        JR Z,IBAADRETA
        DEC A
        JR Z,IBAESQUE
        DEC A
        JR Z,IBAARRIB
IBAABAJO
        LD HL,(PROAB)
        LD (PROABR),HL
        JR EVALUAMOV
IBAADRETA
        LD HL,(PRODE)
        ld (PRODER),HL
        JR EVALUAMOV
IBAESQUE
        LD HL,(PROIZ)
        LD (PROIZR),HL
        JR EVALUAMOV
IBAARRIB
        LD HL,(PROAR)
        LD (PROARR),HL
EVALUAMOV
        LD A,(ALTOSC)
        LD C,A
LOOPROTA:
        PUSH DE
        LD A,(ANCHO)      ;
        LD B,A
BULOOP  LD A,(DE)      ;Coge lo de la pantalla
        AND (HL)          ;Borro lo que no sean 1. Enmascaramiento
        INC HL
        OR (HL)           ;Le sumo el sprite
        LD (DE),A   ;guarda en pantalla
        INC HL
        INC E      ;aumenta posici�n pantalla 1 a la derecha
        DJNZ BULOOP
        POP DE      ;posicion de pantalla original
        INC D      ;aqu� viene la rutina de bajar scan de toda la vida
        LD A,D
        AND 7
        JR NZ,SAL2
        LD A,E
        ADD A,32
        LD E,A
        JR C,SAL2
        LD A,D
        SUB 8
        LD D,A
SAL2    DEC C
        JR NZ,LOOPROTA
;Fin rutina MOV.HL tiene el valor del siguiente frame a imprimir
        LD A,(DIRDMOV)
        AND A
        JR NZ,SCCCP
        LD (PRODE),HL
        RET
SCCCP   DEC A
        JR NZ,SCCCP2
        LD (PROIZ),HL
        RET
SCCCP2  DEC A
        JR NZ,UPARR
        LD (PROAR),HL
        RET
UPARR   LD (PROAB),HL
        RET
;Impresion de atributos del prota.
IMPATRIB:
        call IMP_AT        ;IMPIX.ASM
        LD A,(ALTO)       ;DEL PROTA EN ESTE MOMENTO.
        LD B,A
SIFATTRP:
        PUSH HL
        LD A,(ANCHO)
        LD C,A
        ld a,7            ;COLOR blanco
SIATTRPRO:
        LD (HL),A
        INC HL
        DEC C
        JR NZ,SIATTRPRO
        POP HL
        LD DE,32
        ADD HL,DE          ;FILA DEABAJO
        DJNZ SIFATTRP
        RET