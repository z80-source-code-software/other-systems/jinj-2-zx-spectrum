;###############################
; BUSQUEDA DE PANTALLA E IMPRESION DE DATOS
;###############################
IMP_FONDO:
              ld hl,(VIMPFON)
              jp (hl)
;////////////////////////////////
;fondo NO activado en pantalla

b_impfondo:
              xor a
              call LIMPIOBUFFER
              ld hl, 49487          ;inicio buffer attr
              ld de, 49488
              inc a       ; color de fonod azul oscuro
              ld (hl),a
              ld bc,511
              ldir                  ;15bytes
              ret
;////////////////////////////////
;fondo activado en pantalla
              ld a,(PANTA)
              CP 32
              JR C,PART_ONE
              LD HL,32671  ;PART_TWO
              JR BIMPFON
PART_ONE
              LD HL,24080       ;ORG DE FONTPANT.ASM

BIMPFON       LD A,(HL)
              CP 128
              RET Z
              LD (VE_TILE),A ;GUARDAMOS EL TILE QUE QUEREMOS IMPRIMIR Y QUE DEBEREMOS BUSCAR
              INC HL
              LD A,(HL)
              LD (FIL_DAT),A
              INC HL
              LD A,(HL)
              LD (COL_DAT),A             ;GUARDAMOS FILA Y COLU DEL DATO EN SCANNES PARA CALC_DIR_EN_BUFF_PANT.
              INC HL
              PUSH HL                    ;GUARDAMOS LA ACTUAL POSIC.HL QUE APUNTA AL SIGUIENTE TILE A BUSCAR
              CALL SEARCH_DAT           ;BUSCAMOS EL TILE DE FONDO A INYECTAR EN EL BUFF_PANT.
              CALL C_DIRBUFFTILE         ;CALCULO DE LA DIRECCION EN EL BUFF_INTERMEDIO
              CALL INY_TILE              ;AQUI SALTARIAMOS A LA RUTINA DE INYECCION DE TILE
              CALL C_DIRBUFFTILEATT      ;CALCULO DE LA DIRECCION EN EL BUFF_INTERM_ATTR
              CALL INY_TILEATT           ;RUTINA DE INYECCION DE ATTR DEL TILE
              POP HL                     ;SACAMOS EL VALOR DEL SIGUIENTE TILE A IMPRIMIR Y SEGUIMOS
              JR BIMPFON
