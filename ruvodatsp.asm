; OJO HOY 21/11/2011 
;NO TOQUES LA RUTINA QUE NO CABEN MAS QUE 10BYTES DONDE ESTA. SI LA TOCAS MIRA
; A VER LO QUE OCUPA DENUEVAS. Y/O TRASLADA LA RUNTA RUBORPA.ASM DENUEVO ARRIBA
;////////////////////////////////////////////////////////////////////////////
; Rutina de vuelco de datos TABSPRPAN (SPRITESXPANTALLA) en TABENE
ruvudatspr:                   ;64430
            push ix
            push de
            push hl
            call pri_ruvudatspr
            call seg_ruvudatspr
            pop hl
            pop de
            pop ix
            ret
pri_ruvudatspr:
            ld hl,TABSPRPAN
            ld a,(PANTA)
            ld b,a
bpri_ru     ld a,(hl)
            cp #ff   ;255 para salir
            jr z,bvudatspr
            cp b
            jr z,vudatspr
avudatspr:  ld de,16
            add hl,de    ;siguiente db's
            jr bpri_ru
vudatspr    ld a,l
            ld (TSPRPAN),a
            ld a,h
            ld (TSPRPAN+1),a
            ld a,1
            ld (ENE_EP),a
            ret
bvudatspr   xor a
            ld (ENE_EP),a
            ret
seg_ruvudatspr:
; hl ya tiene el valor de inicio de los enemig. de esa pantalla en TABSPRAN
            LD A,(ENE_EP)
            and a
            ret z           ;Si es 0 es que no hay enemigos en esa pantalla
            ld de,TABENE
bseg_ru     ld a,(hl)
            ld b,a
            ld a,(PANTA)
            cp b
            jr nz,fin_seg_ruvudatspr
            ld bc,16
            ldir
            inc de
            inc de
            jr bseg_ru
fin_seg_ruvudatspr:
            ld ix,TABENE
b_loopfinru
            ld a,(ix+0)
            cp #ff
            jr z,fin_finruvudatspr
            ld a,(ix+6) ;tipene es 1?
            cp 1
            jr nz,bfinruvudatspr  ;si es NORMAL ---> fin
            ld a,(ix+1)
            cp 3
            jr c,bfinruvudatspr   ;si es de mov.horizontal ---> fin
            ld a,(ix+8)
            ld (ix+16),a
            ld a,(ix+9)
            ld (ix+17),a
bfinruvudatspr:
            ld de,18
            add ix,de
            jr b_loopfinru
fin_finruvudatspr
            ret
;/////////////////////////////////////////////////////////////
; Rutina de vuelco de datos SPRITES de TABENE en TABSPRPAN
;
ruvodatabene:
            push ix
            push hl
            push de
            ld ix,TABENE
trasl_dat_ini_ene:
            ld a,(ix+0)
            cp #ff
            jr z,finruvodatabene
            cp 128
            jr z,b_trasl_dat_ini_ene
            ld a,(ix+6)
            cp 1     ;si tipene es 1 entonces pon en movimiento al sprite.
            jr nz,b_b_trasl_dat_ini_ene    ;si no tiene IA continua
            ld a,(ix+1)         ;que clase de anemigo es?
            cp 3
            jr c,eshoriz_ene    ;si es horizontal pon 1 en movene y sigue.
            ld a,(ix+16)    ;posx2
            ld (ix+8),a
            ld a,(ix+17)    ;posy2
            ld (ix+9),a
            call NEWPOSENE  ;calculo ix+12,ix+13
            call C_DIRBUFF_SPRITE
            ld (ix+14),l
            ld (ix+15),h
            jr b_trasl_dat_ini_ene
eshoriz_ene
            ld a,1
            ld (ix+2),a
b_trasl_dat_ini_ene:
            ld de,18
            add ix,de
            jr trasl_dat_ini_ene
b_b_trasl_dat_ini_ene:
            ld a,(ix+1)         ;que clase de anemigo es?
            cp 6
            jr nz,b_trasl_dat_ini_ene      ;caso contrario es eudiparo
            ld a,128
            ld (ix+0),a
            jr trasl_dat_ini_ene
finruvodatabene
            call seg_p_ru_databene
            call cleartabene
            pop hl
            pop de
            pop ix
            ret
seg_p_ru_databene
            ld de,TABENE
            ld a,(TSPRPAN)
            ld l,a
            ld a,(TSPRPAN+1)
            ld h,a
            ex de,hl
bseg_p_ru   ld a,(hl)       ;vuelvo a cargar el primer db's del chorro en curso
            cp 128
            ret z
            cp 255          ;este sobrara
            ret z
            ld bc,16
            ldir
            inc hl
            inc hl
            jr bseg_p_ru
;////////////////////////////////////////////////////////////////////////
; Rutina para vaciar la tabla TABENE
cleartabene:
            xor a
            ld hl,TABENE
            ld de,TABENE+1
            ld (hl),a
            ld bc,71
            ldir
            ld hl,TABENE
            ld a,128
            ld b,4
            ld de,18
lazoctabene
            ld (hl),a
            add hl,de
            djnz lazoctabene
            ret
;-------------------------------------------------------------------------