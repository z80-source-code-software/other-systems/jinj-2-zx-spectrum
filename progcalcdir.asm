        org 32768
;valores de entrada
        ld a,8         ;X
        ld l,21          ;Y
;rutina tabla 4 de metal
        ld h,tablay/256
        add a,(hl)
        inc h
        ld h,(hl)
        ld l,a
        push hl
        pop bc
        ld a,l
        ld (pos_scr),a
        ld a,h
        ld (pos_scr+1),a
C_DIRBUFF:
;            xor a
            LD   E,8       ;x
            LD   D,21      ;DE = fila*32+columna
            LD   HL,45391-1536  ;1536 = 6 chars alto x 32 ancho x 8 bytes  Marcador superior
            ADD  HL,DE   ;TOTAL
            ld a,l
            ld (pos_buff),a
            ld a,h
            ld (pos_buff+1),a
            RET
pos_scr     DEFS 2
pos_buff    DEFS 2
        org 45056
tablay:
        db #00
        db #20
        db #40
        db #60
        db #80
        db #a0
        db #c0
        db #e0
        db #00
        db #20
        db #40
        db #60
        db #80
        db #a0
        db #c0
        db #e0
        db #00
        db #20
        db #40
        db #60
        db #80
        db #a0
        db #c0
        db #e0
        org 45312
        db #40
        db #40
        db #40
        db #40
        db #40
        db #40
        db #40
        db #40
        db #48
        db #48
        db #48
        db #48
        db #48
        db #48
        db #48
        db #48
        db #50
        db #50
        db #50
        db #50
        db #50
        db #50
        db #50
        db #50
