;//////////////////////////////////////////////
;Rutina de interrupciones para el player.
;activado en RUCONTROL.ASM
                org #9b9b       ;39835
intaddr:        jp intmusic
intmusic:
                push af
                push bc
                push de
                push hl
                call INICIO
                pop hl
                pop de
                pop bc
                pop af
                ei
                reti

;-----------------------------------------------------------------------
; Rutina de ISR : incrementa ticks y decrementa timer 10 veces por seg.
;-----------------------------------------------------------------------
;Autor: Santiago Romero adaptada de la Rom disasembled
;       Curso de Ensamblador de Compiler.

intgame:     push af
             PUSH HL
             LD HL,(TIMER)
             DEC HL
             LD (TIMER),HL
             LD A,(TICKS)
             inc a
             ld (TICKS),a
             CP 5
             JR C,FINISR
             XOR A
             LD (TICKS),A
FINISR:      POP HL
             pop af
intnada:     EI
             RETI
