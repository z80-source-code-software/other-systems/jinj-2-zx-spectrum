;ENTRADAS DE LA RUTINA DE INYECCION DE TILES EN EL BUFFER DE PANTALLA INTERMEDIO
;HL= DIR.EN EL BUFFER DE PANTALLA ( ) DEL TILE A GUARDAR.  DATO QUE VIENE DE C_TILEDIRBUFF
;DE= TILE_A GUARDAR
;BC= ALTO Y ANCHO DEL TILE A GUARDAR (FILA Y COLUMNAS)

INY_TILE:

        PUSH HL
        POP DE        ;TRANSFIERO EL VALOR DE HL A DE
        LD A,(POTI_IM)
        LD L,A
        LD A,(POTI_IM+1)
        LD H,A
        LD A,(AL_TILE)
        LD B,A
INY_BU1 PUSH BC
        LD A,(AN_TILE)
        PUSH DE
        LD C,A
        LD B,0
        LDIR          ;TRANSFIERO LOS x SCANNES (POR ANCHURA) DE HL A DE
        POP DE
        LD A,#20      ;SUMAMOS 32 PARA INYECTAR EL SIGUIENTE SCANN EN EL BUFFER
        ADD A,E
        LD E,A
        JR NC,SUMH
        INC D          ;SUMO EL ACARREO A D
SUMH
        POP BC
        DJNZ INY_BU1
        LD A,L               ;Transf.la direcc.a la que apunta HL
        LD (POTI_IA),A       ;que contiene el primer attr del tile
        LD A,H               ;a la variable poti_iatt
        LD (POTI_IA+1),A
        RET
;HEMOS VOLCADO EL TILE X SEGUN LOS DATOS DE LA PANTALLA X EN SU POSICION DENTRO DEL BUFFER INTERMEDIO.
