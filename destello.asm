;//////////////////////////////////////////////////////////
; Rutina de destello que es llamada en las interrupciones Si hay una nota parpadea.
; Entramos con el valor de; VPARPA que es la posicion donde hacer el destello calculada previamente 
destello:
           push hl
           push de
           ld a,(FLAGNOT)
           and a
           jr z,nohagasparpadeo         ; Si no hay salta
           ld a,(D_PARPA)
           cp 4
           jr z,haz_destello
	   ld a,(VPARPA)
	   cp #ff
	   jr z,verotro
	   inc a
	   ld (VPARPA),a
 	   jr bhaz_des
verotro	   ld a,(VPARPA+1)
	   cp #05			;#09 va bien
	   jr z,siverotro
           inc a
           ld (VPARPA+1),a
	   xor a
	   ld (VPARPA),a
           jr bhaz_des
siverotro  ld a,(D_PARPA)
	   inc a
	   ld (D_PARPA),a
	   jr bhaz_des
haz_destello:
           ld hl,#5801
	   xor a
	   ld (D_PARPA),a
           ld a,(COLOR)
           inc a
           cp 8
           jr z,pon_color_a_0
a_pon_color:
           ld (COLOR),a
           ld (hl),a
	   xor a
	   ld (VPARPA),a
	   ld (VPARPA+1),a
           jr bhaz_des
pon_color_a_0:
           xor a
           jr a_pon_color
nohagasparpadeo:
           ld hl,#5801
           ld a,71
           ld (hl),a
bhaz_des   pop de
           pop hl
           ret
