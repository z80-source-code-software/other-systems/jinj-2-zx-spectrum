        org 59800
;Tablas de tuneles y notas musicales
;-----------------------------------

;Notas musicales
NOTAS_PENTA:
        db 0,3,8,15
        db 0,7,14,26
        db 0,8,10,4
        db 0,13,14,12
        db 0,17,12,28
        db 0,19,9,12
        db 0,20,15,2
        db 0,39,18,22             ;pantalla del final
        db 128                   ;29

;Tuneles
;Pantalla de entrada,y=,x=chars
;y salida correspondiente, y=scann, x=chars (coloca al prota, debajo)
;previamente senpro y panta

TUNEL:
        db  0,12, 9, 2,11,19,19
        db  7,17,19, 2,17,19,27
        db  8, 9,27, 2,27,19, 7
        db 15,19, 5, 3, 1,15,18
        db 19,19,15, 0,13,11, 3
        db 27,10,23, 2,11,14, 5
        db 128                    ;43
;si necesitas espacio aqui quita la rutina RUTIMNOT y llevatela antes
;del org INICIAL. Por ejemplo en el archivo ruborpa.ASM
RUTIMNOT:
        ld hl,59800
        ld b,8
        ld a,1
        ld de,4
METEUNO ld (hl),a
        add hl,de
        djnz METEUNO
        ret                       ;15
