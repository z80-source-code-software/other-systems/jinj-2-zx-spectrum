MOVER:
            PUSH BC
            PUSH DE
            PUSH HL
            call Avolcamiento
            CALL MOV           ;VOLCAMOS FRAME DEL PROTA
            POP HL
            POP DE
            POP BC
            RET
NOMOVER:
        LD A,(DIRDMOV)
        AND A
        JR Z,IBAADRETA2
        DEC A
        JR Z,IBAESQUE2
        DEC A
        JR Z,IBAARRIB2
;IBAABAJO
        LD HL,(PROABR)
        LD (PROAB),HL
        JR EVALUAMOV2
IBAADRETA2
        LD HL,(PRODER)
        ld (PRODE),HL
        JR EVALUAMOV2
IBAESQUE2
        LD HL,(PROIZR)
        LD (PROIZ),HL
        JR EVALUAMOV2
IBAARRIB2
        LD HL,(PROARR)
        LD (PROAR),HL
EVALUAMOV2
        CALL IMPATRIB
        CALL MOV
        RET
