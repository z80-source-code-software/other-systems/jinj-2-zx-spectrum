;##################### IMPRESION DE SPRITE's ENEMIGOS
;-------------------------------------------------------------------------
;Entradas: HL Frame sprite DL Posic.pantalla.ANCHOSP= ancho chars A=altoscannes
IMP_ENE:
               ld a,(ix+12)
               ld e,a
               ld a,(ix+13)
               ld d,a      ;de=Posic.en pantalla del sprite
               ld a,(ix+4)
               ld l,a
               ld a,(ix+5)
               ld h,a      ;hl=Sprite enemigo
               call int_impene
               LD C,A      ;Alto en scannes a imprimir (valor desde int_impene
loopsprite:    PUSH DE
               LD A,(ANCHOSP)
               LD B,A
buloopsp       LD A,(DE)      ;Coge el fondo de la pantalla
               AND (HL)       ;Le aplico la mascara. Enmascaramiento
               INC HL
               OR (HL)        ;Le sumo el sprite
               LD (DE),A      ;Lo vuelco en pantalla
               INC HL
               INC E          ;aumenta posici�n pantalla 1 a la derecha
               DJNZ buloopsp
               POP DE      ;posicion de pantalla original
               INC D      ;aqu� viene la rutina de bajar scan de toda la vida
               LD A,D
               AND 7
               JR NZ,sal2sp
               LD A,E
               ADD A,32
               LD E,A
               JR C,sal2sp
               LD A,D
               SUB 8
               LD D,A
sal2sp         DEC C
               JR NZ,loopsprite
               ld a,l   ;Fin rutina HL tiene el valor del siguiente frame a imprimir
               ld (ix+4),a
               ld a,h
               ld (ix+5),a
               ret
;________________________________________
int_impene:
               ld a,(ix+7) ;sentene. Que direccion lleva el enemigo?
               cp 1      ; derecha?
               jr z,dsig_impe
               jr c,bdsig_impe     ;arriba osea vertical
               cp 2
               jr z,bdsig_impe     ;abajo osea vertical si no va a izq.
dsig_impe      ld a,3
               ld (ANCHOSP),A
               dec a
               ld (ALTOSP),a
               ld a,16
               ret
bdsig_impe     LD a,2
               ld (ANCHOSP),A
               INC A
               LD (ALTOSP),A
               ld a,24
               ret
;_____________________ Impresion de atributos del sprite
IMPATRIB_ENE:
               LD A,(ix+9)
               LD B,A   ;FILA 160
               LD A,(ix+8)
               LD C,A   ;COLUMNA 96
               PUSH BC  ;GUARDO EL VALOR PREVIO DE BC PARA IMPRIMIR EL ATTR
               call int_impene
               pop bc
               CALL COMP_ATSP         ;Igual que coordficu (GESTORENE.ASM)
               ld a,(ALTOSP)
               LD B,A       ;Alto del sprite
SIFATTR_sp:    PUSH HL
               LD A,(ANCHOSP)
               LD C,A
SIATTR_sp:     LD (HL),6         ;COLOR amarillo
               INC HL
               DEC C
               JR NZ,SIATTR_sp
               POP HL
               LD DE,32
               ADD HL,DE          ;FILA DEABAJO
               DJNZ SIFATTR_sp
               RET