;/////////////////////////////////////////////////
;rutina de busqueda de tuneles y notas

TUNELADORA:
           PUSH de
           PUSH hl
           LD HL,TUNEL
           LD DE,7
           LD A,(PANTA)
           LD B,A
LOOPTUNEL  LD A,(HL)
           CP 128
           JR Z,TUNELAD
           CP B
           JR Z,TUENCO
LOOPTUNEL2 ADD HL,DE
           JR LOOPTUNEL
TUENCO     LD A,1
           LD (FLAGTUN),A
           ld A,L
           ld (COMPTUN),A      ;Guardo la actual posicion de hl para
           LD A,H              ;comprobar en el juego la posic.del
           LD (COMPTUN+1),A    ;prota con la del tunel de entrada.
TUNELAD    POP hl
           POP de
           RET
;36435
NOTASMU:
           PUSH de
           PUSH hl
           LD HL,NOTAS_PENTA        ;En datgame.asm
           ld de,04
           ld a,(PANTA)
           ld b,a
LOOPNOTA:  LD A,(HL)
           CP 128
           JR Z,RUNOTA
           AND A
           JR Z,LOOPNOTA2
           INC HL     ;de este
           LD A,(HL)
           CP B
           JR Z,NOENCO        ;nota encontrada
           DEC HL     ;este
LOOPNOTA2  add hl,de
           JR LOOPNOTA
NOENCO:    cp 39       ;Pantalla final del juego?
           jr z,levanta_flag_final
           LD A,1
BNOENCO    LD (FLAGNOT),A
           ld A,L
           ld (COMPNOT),A      ;Guardo la actual posicion de hl para
           LD A,H              ;comprobar en el juego la posic.del
           LD (COMPNOT+1),A    ;prota con la de la nota en pantalla.
RUNOTA     POP hl
           POP de
           RET
levanta_flag_final:
           ld a,1
           ld (FLAGFIN),A
           jr BNOENCO
