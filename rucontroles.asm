;#######################
; Rutina de Controles

CONTROLES:
           LD A,247
           IN A,(#FE)
           RRA
           JR NC,KEYBOARD
           RRA
           JR NC,JOYSTICK
           LD A,#DF
           IN A,(#FE)
           rra
           rra
           rra
           RRA
           RRA       ;VER SI PULSADA Y
;           jr nc,CONFONDO
           call nc,CONFONDO
           LD A,#7F
           IN A,(#FE)
           RRA
           RRA
           RRA
           RRA       ;VER SI PULSADA N
;           jr nc,SINFONDO
           call nc,SINFONDO
           JR CONTROLES
KEYBOARD   LD A,123
           LD (#59CB),A
           LD de,38     ;Los bytes que ocupa la rutina joystick
           LD HL,JOY
           add hl,de
           LD A,L
           LD (CONTROL),A
           LD A,H
           LD (CONTROL+1),A
           RET
JOYSTICK
           LD A,123
           LD (#5A0B),A
           LD HL,JOY
           LD A,L
           LD (CONTROL),A
           LD A,H
           LD (CONTROL+1),A
           RET
CONFONDO
           ld de,18    ;bytes que ocupa la rutina b_impfondo en impfondo2.asm
           ld a,22         ;POSICION AT
           rst 16
           ld a,18          ; FILA DONDE EMPIEZA A IMPRIMIR
           rst 16
           ld a,20          ;COLUMNA
           rst 16
           ld a,"y"
           rst 16
           xor a
           ld (W_PAPER),a
           jr BCOSFONDO
SINFONDO
           ld a,22         ;POSICION AT
           rst 16
           ld a,18          ; FILA DONDE EMPIEZA A IMPRIMIR
           rst 16
           ld a,20          ;COLUMNA
           rst 16
           ld a,"n"
           rst 16
           ld a,1
           ld (W_PAPER),a
           ld de,0
BCOSFONDO  ld hl,b_impfondo   ;comienzo de la rutian que NO imprime el fondo
           add hl,de
           ld a,l
           ld (VIMPFON),A
           ld a,h
           ld (VIMPFON+1),A
           ld a,2
           ld (#5A54),a
           dec a              ;SONIDO FX
           LD (REPE_FX),A
           LD A,99
           LD (LONG_FX),A
           CALL FX48K
           CALL NOLOGUAI
           ret
;           jp CONTROLES
