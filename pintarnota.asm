;Datos y rutina de pintado de notas
;***********************************
RESTPUNPINOT:
        ld hl,TABPINOT
        ld a, l
        ld (PPTABNO),a
        ld a, h
        ld (PPTABNO+1),A
        ret

;DATOS
;DIRECCION HEX, POSIC.BIT A IMPRIMIR, 0 PARA SALIR
TABPINOT:
;NOTA 1
         DB #29,#44,200,204,206,0
         DB #29,#45,227,0
         DB #29,#46,2,6,14,30,0
         DB #29,#45,243,251,255,255,0
         DB #29,#58                  ;ATTR'S
;NOTA 2
         DB #2E,#44,32,48,56,0
         DB #2E,#45,143,0
         DB #2E,#46,8,24,56,120,0
         DB #2E,#45,207,239,255,255,0
         DB #2E,#58
;NOTA 3
         DB #30,#44,33,49,57,0
         DB #30,#45,143,0
         DB #30,#46,9,25,57,121,0
         DB #30,#45,207,239,255,255,0
         DB #30,#58
;NOTA 4
         DB #4F,#42,16,24,28,0
         DB #4F,#43,4,0
         DB #4F,#44,199,207,223,255,0
         DB #4F,#43,36,52,60,61,0
         DB #4F,#58
;NOTA 5
         DB #52,#42,65,97,113,0
         DB #52,#43,17,0
         DB #52,#44,31,63,127,255,0
         DB #52,#43,145,209,241,245,0
         DB #52,#58
;NOTA 6
         DB #34,#41,64,96,112,0
         DB #34,#42,31,0
         DB #34,#43,16,48,112,240,0
         DB #34,#42,159,223,255,255,0
         DB #34,#58
;NOTA 7
         DB #5D,#42,8,12,14,0
         DB #5D,#43,2,0
         DB #5D,#44,227,231,239,255,0
         DB #5D,#43,18,26,30,30,0
         DB #5D,#58

PINTARNOTA:
         ld a,1                  ;
         or a                    ;pongo el indicador z a 0
         push hl
         push de
         ld a, (PPTABNO)
         ld l,a
         ld a, (PPTABNO+1)
         ld h,a           ;en hl tengo el puntero de la tabla
         ld a,(hl)
         ld e,a
         inc hl
         ld a,(hl)
         ld d,a
         ld b,23          ;bucle de datos a mover total
buclepi  inc hl
         ld a,(hl)
         and a
         jr z,buc_pinterm
         ld (de),a      ;imprimo en el marcador
         call delaypi
bbuclepi djnz buclepi
buc_pinterm:
         inc hl
         ld a,(hl)
         ld e,a
         inc hl
         ld a,(hl)
         ld d,a
         dec b
         dec b
         ld a,b        ;para saber si es el 0 final.
         and a
         jr nz,bbuclepi
;DE contiene el valor del arch.atributos en este momento

         ld b,10
         ld c,0
bribuc3  push bc
         ld b,255
bribuc2  ld a,119
bribuc   ld (de),a
         dec a
         cp 112
         jr nc,bribuc
         dec b
         ld a,b
         or c
         jr nz,bribuc2
         pop bc
         djnz bribuc3
         inc hl
         ld a, l
         ld (PPTABNO),a
         ld a, h
         ld (PPTABNO+1),A
;Borrar de DATAS_PANT la nota recogida
         ld hl,TABNOTRE
         ld a,(PANTA)
         ld b,a
si_n_bor ld a,(hl)
         cp b
         jr z,borranota
         inc hl
         inc hl
         inc hl
         jr si_n_bor
borranota:
         inc hl
         ld a,(hl)
         ld e,a
         inc hl
         ld a,(hl)
         ld d,a     ;DE tiene el valor de la posicion en DATAS_PANT
         ld a,16    ;Es el valor del tile FONDO 2x2
         ld (de),a
;Ahora borro del buffer
         ld a,(FILA)
         sla a
         sla a
         sla a
         ld (FIL_DAT),A
         ld a,(COLU)
         ld (COL_DAT),A
         ld a,16       ;valor del fondo en DATAS_TILE
         ld (VE_TILE),A
         CALL SEARCH_DAT           ;BUSCAMOS EL TILE DE FONDO A INYECTAR EN EL BUFF_PANT.
         CALL C_DIRBUFFTILE         ;CALCULO DE LA DIRECCION EN EL BUFF_INTERMEDIO
         CALL INY_TILE              ;AQUI SALTARIAMOS A LA RUTINA DE INYECCION DE TILE
         CALL C_DIRBUFFTILEATT      ;CALCULO DE LA DIRECCION EN EL BUFF_INTERM_ATTR
         CALL INY_TILEATT           ;RUTINA DE INYECCION DE ATTR DEL TILE
         LD A,(OBJE)
         DEC A
         LD (OBJE),A
         jr z,antedelay
rrante:
         pop de
         pop hl
         ret
antedelay:
         ld a,128
         ld (FINAL),A              ;esto hace que pases de fase
         jr rrante
delaypi:
         push bc
         LD B,1
bu1deypi PUSH BC
         LD B,255
bu2deypi DJNZ bu2deypi
         POP BC
         DJNZ bu1deypi
         pop bc
         ret
TABNOTRE
         DB 3,#98,#C4
         DB 7,#E7,#C5
         DB 8,#46,#C6
         DB 13,#D5,#C8
         DB 17,#51,#CA
         DB 19,#09,#CB
         DB 20,#9E,#CB