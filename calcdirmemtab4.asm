;Tablas para el calculo de direcciones en pantalla adaptadas a la tabla-4
; de metalbrain separadas ambas (dblow del dbhigh) 256bytes.
        org 45056
tablay:
        db #00,#20,#40,#60,#80,#a0,#c0,#e0
        db #00,#20,#40,#60,#80,#a0,#c0,#e0
        db #00,#20,#40,#60,#80,#a0,#c0,#e0

;Rutina de tablas para el calculo de coord. en pantalla by metalbrain
; entrada a=X y l=Y
tabla4metal:
                    ld h,tablay/256
                    add a,(hl)
                    inc h
                    ld h,(hl)
                    ld l,a
                    ret
;RUTINA DE CALCULO DE DIRECCION EN BUFFER DADAS LA FILA Y LA COLUMNA sprite
;ENTRADA:FILA,COLU en chars. SALIDA:POS.INICIO DEL SPRITE EN BUFFER PANTALLA
;------------------------------------------------------------------
;esta rutina se incluye para aprovechar los bytes entre las tablas

C_DIRBUFF_SPRITE:
                    LD   A,(IX+8)
                    LD   E,A
                    LD   A,(IX+9)
                    LD   D,A      ;DE = fila,columna
                    LD   HL,45391-1536  ;1536 = 6 chars alto x 32 ancho x 8 bytes  Marcador superior
                    ADD  HL,DE   ;TOTAL
                    RET
        org 45312
        db #40,#40,#40,#40,#40,#40,#40,#40
        db #48,#48,#48,#48,#48,#48,#48,#48
        db #50,#50,#50,#50,#50,#50,#50,#50