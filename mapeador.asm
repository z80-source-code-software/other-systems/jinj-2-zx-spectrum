;#######################
;  RUTINA DE MAPEADO
;#######################
; En la llamada a esta rutina traeremos una variable para saber en que posicion estaba el prota, osea el limite
; de pantalla al que ha llegado. ARRIBA,DER,ABAJO,IZQ. (0,1,2,3). Vamos a 'crear' un byte con la informaci�n
; suficiente que necesita un offset en una tabla.
; Para la direcci�n a la que va a ir tomamos el bit 7 y 6 (valores 00 01 10 11) usea arriba,der,abajo,izq
; Para el n�mero de pantalla el resto de bits de la variable PANTA (m�ximo de pantallas 64)
; La variable que sabe el sentido del prota la llamamos SENPRO.
MAPPING
            LD A,(PANTA)
            LD (LAST_SE),A  ; GUARDO EL VALOR ACTUAL DE PANTA PARA USARLO SI MUERO.
            LD B,A
            LD A,(SENPRO)
            RRCA
            RRCA          ;TENGO EL VALOR EN LOS BITS 7 Y 6
            OR B          ;ACABO DE METER PANTA JUNTO A SENPRO
            LD B,A
            LD DE,TABSEN  ;TABLA CON 64X4 = 256  POSIBLES DIRECCIONES
SALTARIN    LD A,(DE)
            CP B
            JR Z,SALTAB
            INC DE        ;COMPROBAMOS EL SIGUIENTE
            INC DE
            JR SALTARIN   ;OJO COMO TE EQUIVOQUES Y NO ENCUENTRE NADA SE CUELGA AQUI.
SALTAB      INC DE
            LD A,(DE)     ;NUEVO VALOR DE LA VARIABLE PANTA
            LD (PANTA),A  ;AL ENCONTRAR LA CORRESPONDENCIA ENCUENTRAS LA PANTALLA A LA QUE QUIERES IR.
            RET
;#########################
;  FIN RUTINA DE MAPEADO
;#########################
;TABLA DE DIRECCIONES DE SALIDA DE PANTALLA. MAPEADO
TABSEN      
            db %01000000,1
            db %10000000,4
            db %01000001,2
            db %10000001,5
            db %11000001,0
            db %01000010,3
            db %10000010,6
            db %11000010,1
            db %00000011,32
            db %10000011,7
            db %11000011,2
            db %00000100,0
            db %01000100,5
            db %10000100,8
            db %00000101,1
            db %01000101,6
            db %10000101,9
            db %11000101,4
            db %00000110,2
            db %01000110,7
            db %10000110,10
            db %11000110,5
            db %00000111,3
            db %10000111,11
            db %11000111,6
            db %00001000,4
            db %01001000,9
            db %10001000,12    ;            DEFB %00000000,0   SENPRO=2 panta=8
            db %00001001,5
            db %01001001,10
            db %10001001,13
            db %11001001,8
            db %00001010,6
            db %01001010,11
            db %10001010,14
            db %11001010,9
            db %00001011,7
            db %10001011,15
            db %11001011,10
            db %00001100,8
            db %01001100,13
            db %10001100,16
            db %00001101,9
            db %01001101,14
            db %10001101,17
            db %11001101,12
            db %00001110,10
            db %01001110,15
            db %10001110,18
            db %11001110,13
            db %00001111,11
            db %10001111,19
            db %11001111,14
            db %00010000,12
            db %01010000,17
            db %10010000,20
            db %00010001,13
            db %01010001,18
            db %10010001,21
            db %11010001,16
            db %00010010,14
            db %01010010,19
            db %10010010,22
            db %11010010,17
            db %00010011,15
            db %10010011,23
            db %11010011,18
            db %00010100,16
            db %01010100,21
            db %10010100,24
            db %00010101,17
            db %01010101,22
            db %10010101,25
            db %11010101,20
            db %00010110,18
            db %01010110,23
            db %10010110,26
            db %11010110,21
            db %00010111,19
            db %10010111,27
            db %11010111,22
            db %00011000,20
            db %01011000,25
            db %10011000,28
            db %00011001,21
            db %01011001,26
            db %10011001,29
            db %11011001,24
            db %00011010,22
            db %01011010,27
            db %10011010,30
            db %11011010,25
            db %00011011,23
            db %10011011,31
            db %11011011,26
            db %00011100,24
            db %01011100,29
            db %00011101,25
            db %01011101,30
            db %11011101,28
            db %00011110,26
            db %01011110,31
            db %11011110,29
            db %00011111,27
            db %11011111,30

            db %01100000,33
            db %10100000,3

            db %11100001,32
            db %10100001,34

            db %00100010,33
            db %10100010,35

            db %00100011,34
            db %10100011,36

            db %00100100,35
            db %10100100,44
            db %01100101,38
            db %10100101,39
            db %11100110,37
            db %10100110,40
            db %00100111,37
            db %01100111,40
            db %10100111,41
            db %00101000,38
            db %10101000,42
            db %11101000,39
            db %00101001,39
            db %01101001,42
            db %10101001,46
            db %00101010,40
            db %10101010,47
            db %11101010,41
            db %10101011,45
            db %00101100,36
            db %01101100,45
            db %10101100,48
            db %00101101,43
            db %01101101,46
            db %10101101,49
            db %11101101,44
            db %00101110,41
            db %01101110,47
            db %10101110,50
            db %11101110,45
            db %00101111,42
            db %10101111,51
            db %11101111,46
            db %00110000,44
            db %01110000,49
            db %10110000,52
            db %00110001,45
            db %01110001,50
            db %10110001,53
            db %11110001,48
            db %00110010,46
            db %01110010,51
            db %10110010,54
            db %11110010,49
            db %00110011,47
            db %10110011,55
            db %11110011,50
            db %00110100,48
            db %01110100,53
            db %10110100,56
            db %00110101,49
            db %01110101,54
            db %10110101,57
            db %11110101,52
            db %00110110,50
            db %01110110,55
            db %10110110,58
            db %11110110,53
            db %00110111,51
            db %10110111,59
            db %11110111,54
            db %00111000,52
            db %01111000,57
            db %00111001,53
            db %01111001,58
            db %10111001,60
            db %11111001,56
            db %00111010,54
            db %01111010,59
            db %10111010,61
            db %11111010,57
            db %00111011,55
            db %11111011,58
            db %00111100,57
            db %01111100,61
            db %10111100,62
            db %00111101,58
            db %10111101,63
            db %11111101,60
            db %00111110,60
            db %01111110,63
            db %00111111,61
            db %11111111,62


