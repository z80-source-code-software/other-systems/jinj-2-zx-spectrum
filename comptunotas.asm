comprob_tunotas:
        LD A,(FLAGTUN)
        AND A
        CALL NZ,COMPTUNEL  ;Si el flag est� a 1 comprobar tuneles
        LD A,(FLAGNOT)
        AND A
        CALL NZ,COMPNOTAS  ;Si el flag est� a 1 comprobar notas
        RET
;Tuneles entre arboles
COMPTUNEL:   PUSH HL                          ;(36083)
             LD A,(COMPTUN)
             LD L,A
             LD A,(COMPTUN+1)
             LD H,A
             INC HL
             LD A,(HL)
             LD B,A
             LD A,(FILA)
             CP B
             JR Z,FIENT
             POP HL
             RET
FIENT:       INC HL
             LD A,(HL)
             LD B,A
             LD A,(COLU)
             CP B
             JR Z,COENT
             POP HL
             ret
COENT        call delay
             LD DE,PROTU1
             ld a,e
             ld (PROAR),a
             ld a,d
             ld (PROAR+1),a
             CALL MOVER
             call delay
             LD DE, PROTU2
             ld a,e
             ld (PROAR),a
             ld a,d
             ld (PROAR+1),a
             CALL MOVER
             call delay
             LD DE, PROTU3
             ld a,e
             ld (PROAR),a
             ld a,d
             ld (PROAR+1),a
             CALL MOVER
             call delay
             XOR A
             LD (FLAGTUN),A
             ld (FRAMEV),A
             INC A
             LD (FLAGPA),A     ;FLAG DE CAMBIO DE PANTALLA ACTIVADO
             ld (PRENTU),A     ;ACTIVO FLAG DE TRANSICION ENTRETUNELES
             inc hl            ;
             ld a,(hl)
             ld (SENPRO),A
             inc hl
             ld a,(hl)
             LD (LAST_SE),A
             ld (PANTA),A
             inc hl
             ld a,(hl)
             ld (LAST_P+1),A

             ld (FILA),A
             inc hl
             ld a,(hl)
             LD (LAST_P),A

             ld (COLU),A
             POP HL
             ld hl,tab_fx_tun   ;LLAMADA AL SONIDO
             ld b,8      ;bucle de repeticion
loop_tun     ld a,(hl)
             ld (REPE_FX),a
             inc hl
             ld a,(hl)
             ld (LONG_FX),a
             inc hl
             call FX48K
             djnz loop_tun
             ld de,PROTAAB       ;actualizo frame prota
             ld a,e
             ld (PROAB),a
             ld a,d
             ld (PROAB+1),a
             ld a,3
             LD (DIRDMOV),A
             ret
delay
             push bc
             LD b,1
subdelay     push bc
             halt
             ld bc,0
delayer      djnz delayer
             pop bc
             djnz subdelay
             pop bc
             ret
;notas musicalds en pantalla
COMPNOTAS:   PUSH HL                           ;(36231)
             LD A,(COMPNOT)
             LD L,A
             LD A,(COMPNOT+1)
             LD H,A
             PUSH HL         ;Guardo para poner 0 si recoges la nota
             INC HL
             LD A,(HL)
             LD B,A
             LD A,(FILA)
             CP B
             JR Z,FIGUA
             POP HL
             POP HL
             RET
FIGUA:       INC HL
             LD A,(HL)
             LD B,A
             LD A,(COLU)
             CP B
             JR Z,COGUA
             POP HL
             POP HL
             ret
COGUA        ld a,(FLAGFIN)
             and a
             jr z,BCOGUA
             xor a      ;Haz llegado al final del juego
             ld (FINAL),a
             pop hl
             pop hl
             ret         ;para que no se cuelgue en pintarnota al llegar al final
BCOGUA        call delay
             ld hl,tab_fx_not  ;LLAMADA AL SONIDO
             ld b,2      ;bucle de repeticion
loop_not     ld a,(hl)
             ld (REPE_FX),a
             inc hl
             ld a,(hl)
             ld (LONG_FX),a
             inc hl
             call FX48K
             djnz loop_not
             ld a,16
             ld (ALTOSC),A
             LD A,2
             LD (ANCHO),A
             LD (ALTO),A
             inc a
             LD (DIRDMOV),A
             LD DE,PRONOTA1               ;(36551)
             ld a,e
             ld (PROAB),a
             ld a,d
             ld (PROAB+1),a
             CALL MOVER
             call delay
             LD DE,PRONOTA2
             ld a,e
             ld (PROAB),a
             ld a,d
             ld (PROAB+1),a
             CALL MOVER
             call delay
             LD DE,PRONOTA3
             ld a,e
             ld (PROAB),a
             ld a,d
             ld (PROAB+1),a
             CALL MOVER
             call delay
;imprimo al prota mirando al frente.
             ld de,PROTAAB
             ld a,e
             ld (PROAB),a
             ld a,d
             ld (PROAB+1),a
             CALL MOVER
             call delay
             pop hl        ;saco el valor de hl en NOTA: (DATGAMEN.ASM)
             dec hl
             xor a
             ld (hl),a     ;pongo 0 en la nota al ser recogida
             LD (FLAGNOT),A
             POP HL
             call PINTARNOTA      ;PINTARNOTA.ASM para imprimir las notas en el marcador
             RET
tab_fx_not
             db 1,125
             db 1,155
tab_fx_tun
        db 4,85
        db 3,55
        db 2,25
        db 1,10
        db 5,50
        db 2,85
        db 3,135
        db 1,255
