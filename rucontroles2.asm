;#######################
; Rutina de Controles

CONTROLES:
           LD A,247
           IN A,(#FE)
           RRA
           JR NC,KEYBOARD
           RRA
           JR NC,JOYSTICK
           LD A,#DF
           IN A,(#FE)
           rra
           rra
           rra
           RRA
           RRA       ;VER SI PULSADA Y
           call nc,CONFONDO
           LD A,#7F
           IN A,(#FE)
           RRA
           RRA
           RRA
           RRA       ;VER SI PULSADA N
           call nc,SINFONDO
           JR CONTROLES
KEYBOARD   LD A,123
           LD (#59CB),A
           LD de,38     ;Los bytes que ocupa la rutina joystick
           LD HL,JOY
           add hl,de
           LD A,L
           LD (CONTROL),A
           LD A,H
           LD (CONTROL+1),A
           RET
JOYSTICK
           LD A,123
           LD (#5A0B),A
           LD HL,JOY
           LD A,L
           LD (CONTROL),A
           LD A,H
           LD (CONTROL+1),A
           RET
CONFONDO
           xor a
           ld (W_PAPER),a
           ld hl,texty
           ld de,#5054
           ld b,8
b_confon   ld a,(hl)
           ld (de),a
           inc hl
           inc d
           djnz b_confon
           ld de,18    ;bytes que ocupa la rutina b_impfondo en impfondo2.asm
           jr BCOSFONDO
SINFONDO
           ld a,1
           ld (W_PAPER),a
           ld hl,textn
           ld de,#5054
           ld b,8
s_confon   ld a,(hl)
           ld (de),a
           inc hl
           inc d
           djnz s_confon
           ld de,0
BCOSFONDO  ld hl,b_impfondo   ;comienzo de la rutian que NO imprime el fondo
           add hl,de
           ld a,l
           ld (VIMPFON),A
           ld a,h
           ld (VIMPFON+1),A
           ld a,2
           ld (#5A54),a
           dec a              ;SONIDO FX
           LD (REPE_FX),A
           LD A,99
           LD (LONG_FX),A
           CALL FX48K
           CALL NOLOGUAI
           ret
texty      DEFB	 66,199, 66, 62, 14, 38, 38, 28

textn      DEFB	  0, 64,228,252,118, 98, 98,102
