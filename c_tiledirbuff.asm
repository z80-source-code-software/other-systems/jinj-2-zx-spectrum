;VAMOS A IMPRIMIR NO EN LA PANTALLA SI NO EL BUFFER CADA PANTALLA DEL JUEGO
;PRIMERO VOLCAREMOS LOS DATOS DE LA TABLA DE PANTALLAS AL BUFFER INTERMEDIO

;RUTINA DE CALCULO DE LA DIRECCION EN EL BUFFER INTERMEDIO DADAS
;ENTRADA:fila en pixels y columna en chars DE LOS TILES A IMPRIMIR
;SALIDA:POSICION DE INICIO DEL TILE A IMPRIMIR EN EL BUFFER DE PANTALLA

C_DIRBUFFTILE:
            LD   HL,45391      ;antes 24580  
            LD   A,(FIL_DAT)
            SUB 48          ;para saber si est�s en la fila 0 del area de juego.
            JR   Z,SUMACOLUTI
            LD   D,0
            LD   E,A
            SLA E         ;MULTIPLICAMOS POR 32 (CINCO VECES CORRER A LA IZQ. DE
            RL D
            SLA E
            RL D
            SLA E
            RL D
            SLA E
            RL D
            SLA E
            RL D
            ADD  HL,DE    ;YA TENEMOS EL VALOR DE LA FILA SUMADO
SUMACOLUTI  LD   A,(COL_DAT)
            LD   E,A
            LD   D,0
            ADD  HL,DE   ;SUMO LA COLUMNA
            RET
;ENTRADA:fila en pixels y columna en chars DE LOS TILES A IMPRIMIR
;SALIDA:POSICION DE INICIO DE LOS ATTR DEL TILE A IMPRIMIR EN EL BUFFER

C_DIRBUFFTILEATT:
            LD   HL,49487  ;antes Inicio buffer attr
            LD   A,(FIL_DAT)
            SUB 48          ;para saber si est�s en la fila 0 del area de juego.
            JR   Z,SUMACOLUATT
            LD   D,0
            LD   E,A
;PARA CALCULAR LOS CHARS DEBEMOS 1� DIVIDIR ENTRE 8 LOS SCANNES FILA Y DESPUES
;MULTIPLICAR POR 32 (CADA FILA TIENE 32) ES LO MISMO QUE SI MULTIPLICAMOS 4 DE PRIMERAS.
            SLA E         ;MULTIPLICAMOS POR 4 (DOS VECES CORRER A LA IZQ. DE
            RL D
            SLA E
            RL D
            ADD  HL,DE    ;YA TENEMOS EL VALOR DE LA FILA SUMADO
SUMACOLUATT LD   A,(COL_DAT)
            LD   E,A
            LD   D,0
            ADD  HL,DE   ;SUMO LA COLUMNA
            RET
;TENGO EN HL LA POSICION EN EL BUFFER DE ATTR DONDE GUARDAR EL COLOR



