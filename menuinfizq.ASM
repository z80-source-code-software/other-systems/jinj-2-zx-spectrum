
; menuinfizq
MENUINFIZQ:
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0
	DEFB	  0,  0,  0,  2,168,  0,  0,  3
	DEFB	  0,  0
	DEFB	  0,  0,  4,  5,255,  0,  0,  7
	DEFB	128,  0
	DEFB	  0,  0,  8, 43,255,192, 55, 15
	DEFB	192,  0
	DEFB	  0,  0,  4, 87,255,224,255,137
	DEFB	  0,  0
	DEFB	  0,  0,  0, 47,255,243,255,224
	DEFB	  0,  0
	DEFB	  0,  0,  0, 95,255,255,249,192
	DEFB	  0,  0
	DEFB	  0,  0,  1,159,255,255,224,  0
	DEFB	  0,  0

	DEFB	  0,  0,  1,127,255,254,  0,  0
	DEFB	  0,  0
	DEFB	  0,  0,  0,175,255,248,  0,  0
	DEFB	  0,  0
	DEFB	  0,  0,  1,127,253,116,  0,  0
	DEFB	  0,  0
	DEFB	  0,  0,  0,173,250,168,  0,  1
	DEFB	240,  0
	DEFB	  0,  0,  0, 91,252, 80,  0,  7
	DEFB	 16,  0
	DEFB	  0,  0,  0,156,242,  0,  0, 14
	DEFB	  0,  0
	DEFB	  0,  0,  1, 76,232,  0,  0, 48
	DEFB	  0,  0
	DEFB	  0, 26,162,149,192,  0,  0,108
	DEFB	  0,  0

	DEFB	  0,215,213,171,224,  0,  0,216
	DEFB	  0,  0
	DEFB	  3,191,254,143,208,  0,  1,192
	DEFB	  0,  0
	DEFB	  2,225, 95,195,232,  0,  3,128
	DEFB	  0,  0
	DEFB	  7,128,111,199,252,  0,  7,192
	DEFB	  0,  0
	DEFB	  5,  0, 31,242,254,  0, 14,128
	DEFB	  0,  0
	DEFB	  7,  0, 15,241,254,128, 31,  0
	DEFB	  0,  0
	DEFB	  6,  0, 31,233,253,  0, 30,  0
	DEFB	  0,  0
	DEFB	  4,  0, 15,224,255,160, 61,  0
	DEFB	  0,  0

	DEFB	  6,  0,  7,248,255,240,122,  0
	DEFB	193,  0
	DEFB	  7,  0, 31,248,127,224,124, 15
	DEFB	 62,  0
	DEFB	  2,  0, 47,252, 63,249,250, 63
	DEFB	152,  0
	DEFB	  3,  0, 31,252,215,255,253, 95
	DEFB	  0,  0
	DEFB	  1,  0, 47,228, 97,255,250,254
	DEFB	  0,  0
	DEFB	  1,128, 95,250, 12,251,245, 93
	DEFB	  0,  0
	DEFB	 12,138,191,254,  0,254,171,126
	DEFB	160, 30
	DEFB	255, 85,127,234,211,213, 85,255
	DEFB	 92,251



; (Attributes here, insert label if needed)

	DEFB	  2,  2, 70, 66, 66, 66, 66, 66
	DEFB	  2,  2
	DEFB	  2,  2,  2,114, 66,  2,  2, 66
	DEFB	 66,  2
	DEFB	  2,  2, 66,114, 66,  2, 66, 66
	DEFB	  2,  2
	DEFB	  2,  2, 66,114,114, 66, 66,  2
	DEFB	  2,  2
