; ENTRADA DE RUTINA HL=VALOR DE LA POSICION DEL SPRITE EN EL BUFFER DE PANTALLA
; DE= VALOR DEL SPRITE EN PANTALLA,ALTO EN SCANNES Y ANCHO EN CHARS
; DE=DESTINO EN PANTALLA. HL=INIC.BUFFER+POSIC SPRITE.

Avolcamiento :

            CALL IM_PIX       ;SALIDA HL POSIC.ARCH.PANTALLA
            PUSH HL           ;GUARDO LA POSIC.
            CALL C_DIRBUFF    ;HL TIENE AHORA EL VALOR DEL FONDO DE TAMA�O EL SPRITE
            POP DE            ;SACO EN DE LA POSIC.EN ARCH.PANTALLA
;HL = Inicio del buffer y DE = A la posicion en el archivo de pantalla
            ld a,(ALTOSC)
            ld b,a
Lazoavolc1  push bc
            push de
            push hl
            ld a,(ANCHO)
            ld b,a
Lazoavolc2  ld a,(hl)
            ld (de),a
            inc hl
            inc de
            djnz Lazoavolc2
            pop hl
            ld c,#20        ;B ES 0 YA
            add hl,bc        ;inmediato de debajo
            pop de           ;saco el arch.de pantalla
            pop bc           ;siguiente scann a imprimir
            dec b
            ret z            ;si es el �ltimo retorna
            inc d
            LD    A,D
            AND   7
            JR    NZ,Lazoavolc1  ;baja una l�nea dentro del caracter
            LD    A,#20
            ADD   A,E
            LD    E,A
            JR    C,Lazoavolc1    ;cambio de tercio
            LD    A,D
            SUB   8
            LD    D,A
            JR    Lazoavolc1       ;cambio de caracter
