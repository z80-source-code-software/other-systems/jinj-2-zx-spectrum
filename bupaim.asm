;###############################
; BUSQUEDA DE PANTALLA E IMPRESION DE DATOS
;###############################
SEARCH_SCR:
              LD A,(PANTA)
              LD HL,TABPANT   ;DATOS DE LAS DIRECCIONES DONDE ESTAN LAS PANTALLAS.
              ADD A,A
              ADD A,L
              JR NC,CONSEARCH
              INC H           ;En el caso de acarreo sumalo para que calcule correctamente
                              ;la posicion en la tabla de direcciones de pantalla.
CONSEARCH:
              LD L,A
              LD A,(HL)
              LD E,A
              INC HL
              LD A,(HL)
              LD D,A
              PUSH DE
              POP HL          ;TRASPASO EL VALOR DE A HL= DIR.PANTALLA A IMPRIMIR
                              ;LOS VALORES SIGUIENTES SERAN LOS TILES QUE COMPONEN
              INC HL          ;LA PANTALLA Y SU POSC.X E Y EN SCANNES
A_FRM         LD A,(HL)
              CP 128          ;ROMPE LA PANTALLA AL ENCONTRAR CODIGO 128
              RET Z           ;FIN PANTALLA
              LD (VE_TILE),A ;GUARDAMOS EL TILE QUE QUEREMOS IMPRIMIR Y QUE DEBEREMOS BUSCAR
              INC HL
              LD A,(HL)
              LD (FIL_DAT),A
              INC HL
              LD A,(HL)
              LD (COL_DAT),A             ;GUARDAMOS FILA Y COLU DEL DATO EN SCANNES PARA CALC_DIR_EN_BUFF_PANT.
              INC HL
              PUSH HL                    ;GUARDAMOS LA ACTUAL POSIC.HL QUE APUNTA AL SIGUIENTE TILE A BUSCAR
              CALL SEARCH_DAT            ;BUSCAMOS EL TILE A INYECTAR EN EL BUFF_PANT EN LA TABLA DE DATOS.
              CALL C_DIRBUFFTILE         ;CALCULO DE LA DIRECCION EN EL BUFF_INTERMEDIO
              CALL INY_TILE              ;AQUI SALTARIAMOS A LA RUTINA DE INYECCION DE TILE
              CALL C_DIRBUFFTILEATT      ;CALCULO DE LA DIRECCION EN EL BUFF_INTERM_ATTR
              CALL INY_TILEATT           ;RUTINA DE INYECCION DE ATTR DEL TILE
              POP HL                     ;SACAMOS EL VALOR DEL SIGUIENTE TILE A IMPRIMIR Y SEGUIMOS
              JR A_FRM
;###############################################################
; SUBRUTINA DE BUSQUEDA DE LOS DATOS QUE COMPONEN LAS PANTALLAS
;###############################################################

SEARCH_DAT    LD DE,TABDATA
AHIBA         LD A,(VE_TILE)
              LD B,A
              LD A,(DE)
              CP B
              JR Z,ENCONTRADO
;En el caso de que no lo encuentre debo saber que tama�o tiene para seguir con el siguiente tile
              INC DE                 ;BUSCAMOS EL SIGUIENTE TILE EN LA TABLA TABDATA
              LD A,(DE)              ;FILA X 8 X COLUMNA
              LD L,A                 ;GUARDO LA FILA
              INC DE
              LD A,(DE)
              LD B,A
              CP 1                   ;SI HAY UNA SOLA COLUMNA SALTO YA QUE EL VALOR YA LO TENGO
              JR Z,AN_METRO
              DEC B                  ;DECREMENTO LA COLUMNA PARA HACER EL CALCULO BIEN.
              XOR A                  ;PONGO A 0 EL ACUMULADOR Y EL ACARREO
              LD H,A
              LD A,L
SUM_XV_COL    ADD A,L
              RL H		     ;EN EL CASO DE ACARREO H LO VA A IR ASIMILANDO	
              DJNZ SUM_XV_COL        ;AQUI HL=AL VALOR A SUMAR A DE PARA OBTENER EL SIGUIENTE TILE
              LD L,A
              JR METRO
AN_METRO      LD H,0

METRO         PUSH HL
	      SRL H
	      RR L
	      SRL H
	      RR L
              SRL H
              RR L		     ;Division entre 8
	      LD A,L		     ;L contiene la division = cantidad de ATTR's del tile
              POP HL
	      ADD HL,DE
	      LD D,0
	      LD E,A                 ;
	      ADD HL,DE		     ;SUMO LOS ATTRS DEL TILE
              INC HL                 ;SIGUIENTE TILE A COMPROBAR
              PUSH HL
              POP DE                 ;DE TIENE EL NUEVO VALOR DENTRO DE TABDATA
              JR AHIBA
ENCONTRADO    INC DE
              LD A,(DE) ;ALTO
              LD (AL_TILE),A        ;ALTURA DEL TILE EN SCANNES PARA HACER LDIR
              INC DE
              LD A,(DE) ;ANCHO       ;ANCHURA DEL TILE EN CARAS PARA HACER LDIR
              LD (AN_TILE),A
              INC DE                 ;PRIMER BYTE A IMPRIMIR/INYECTAR EN EL BUFF_PANT
              LD A,E
              LD (POTI_IM),A
              LD A,D
              LD (POTI_IM+1),A      ;GUARDO EN POTI_IMP LA POSC.DEL TILE A IMPRIMIR DENTRO DE TABDATA
              RET
