;#######################################################################
;Rutinas de volcado del attr del fondo para los sprites enemigos

;_____ Rutina de Calculo de la dir.en el buff de attr __________________
;ENTRADA:fila y colu en chars.
;SALIDA: HL POSICION EN BUFFER DE ATTR DEL ENEMIGO
C_DIRBUFFATT_ENE:
            AND A       ;Acarreo a 0
            LD   HL,49487  ;antes Inicio buffer attr
            LD   A,(IX+9)  ;POSY del enemigo (FILA)
            SUB 6          ;para saber si est�s en fila 0 del area de juego
            JR   Z,SUM_COL_ENE
            LD   E,A
            LD   D,0
            SLA E
            RL D
            SLA E
            RL D
            SLA E
            RL D
            SLA E
            RL D
            SLA E
            RL D          ;Multiplicamos x 32
            ADD  HL,DE    ;YA TENEMOS EL VALOR DE LA FILA SUMADO
SUM_COL_ENE LD A,(IX+8)   ;POSX del enemigo (COLU)
            LD   E,A
            LD   D,0
            ADD  HL,DE   ;SUMO LA COLUMNA
            RET

;___ rutina equivalencias entre buffer y memoria de atributos __________
INT_VAL_BUFF:              ;HL tiene la direccion en el buffer de atributos.
        PUSH HL          ;Guardo el valor de HL buffer attr
        LD A,(IX+9)  ;POSY
        LD B,A
        LD A,(IX+8)
        LD C,A
        CALL COMP_ATSP    ;Rutina en GESTORENE.ASM (para ahorrar bytes)
        EX DE,HL          ;Interc.valores DE tiene el valor en el arch.attr
        POP HL            ;Saco el valor del buffer de attr.
        RET
;______ Vuelca los atrib. del fondo del enemigo al moverse en horizontal__
VOL_ATTR_F_ENE_HOR:
        CALL C_DIRBUFFATT_ENE  ;salida HL= valor del  buffer attr
        CALL INT_VAL_BUFF
        LD A,(HL)
        LD (DE),A
        call calc_tile_inf
        LD A,(HL)       ;segunda fila
        LD (DE),A
        ret
;______ mismo que anterior pero para enemig.volanteros con IA de 3x2
VOL_ATTR_F_ENE_HOR_IA:
        CALL C_DIRBUFFATT_ENE  ;salida HL= valor del  buffer attr
        CALL INT_VAL_BUFF
        LD A,(HL)
        LD (DE),A
        call calc_tile_inf
        LD A,(HL)       ;segunda fila
        LD (DE),A
        call calc_tile_inf
        LD A,(HL)       ;tercera fila
        LD (DE),A
        ret

calc_tile_inf
        ld bc,#20
        add hl,bc
        push hl
        ld hl,#20
        add hl,de
        push hl
        pop de
        pop hl
        RET
;______ Vuelca los atrib. del fondo del enemigo al moverse en vertical__

VOL_ATTR_F_ENE_VER:
        CALL C_DIRBUFFATT_ENE  ;salida HL= valor del  buffer attr
        CALL INT_VAL_BUFF
        LD A,(HL)
        LD (DE),A
        INC HL
        INC DE
        LD A,(HL)
        LD (DE),A
        RET
;______ Vuelca los atributos del fondo del enemigo total _______________
VOL_ATTR_F_ENE_TOTAL:
        CALL C_DIRBUFFATT_ENE  ;salida HL= valor del  buffer attr
        CALL INT_VAL_BUFF
        LD B,2         ;Para jinj2 siempre son 2 el alto del disparo
BVARENB PUSH BC        ;esta rutina viene del prota y puede adaptarse
        PUSH HL        ;para cualquier tama�o en ancho y alto, solo
        PUSH DE        ;cargando en a la variable correspondiente y
        LD B,0         ;pasando el valor de "a"  a "b" y "c "
        LD C,3         ;Para jinj2 siempre son 2 el ancho del disparo
        LDIR
        POP DE
        POP HL
        ld bc,#20
        add hl,bc
        push hl
        ld hl,#20
        add hl,de
        push hl
        pop de
        pop hl
        POP BC
        DEC B
        JR NZ,BVARENB
        RET
