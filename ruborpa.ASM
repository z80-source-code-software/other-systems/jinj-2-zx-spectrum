;//////////////////////////////////////////////////////////////////////////
;              Rutina de borrado de pantalla completo

RUBORCO
           LD HL,#4000
           LD DE,#4001
           LD (HL),l
           LD BC,6144+767
           LDIR
           RET
;//////////////////////////////////////////////////////////////////////////
;              Rutina de borrado de pantalla de juego
; tama�o 32 ancho por 16 alto.
rubo_game
           ld hl,#40c0
           ld c,128
brubo_game push hl
           ld b,32
           xor a
b_brubo_game
           ld (hl),a
           inc l
           djnz b_brubo_game
           pop hl
           inc h      ;aqu� viene la rutina de bajar scan de toda la vida
           ld a,h
           and 7
           jr nz,rubo_game_sal
           ld a,l
           add a,32
           ld l,a
           jr c,rubo_game_sal
           ld a,h
           sub 8
           ld h,a
rubo_game_sal
           dec c
           jr nz,brubo_game
           ret

;VACIADO DE buffer de PANTALLA Y RESTAURACION; 
LIMPIOBUFFER
        LD HL,45391
        LD DE,45392
        LD BC,4608	;DATAS + ATTR
        LD (HL),A
        LDIR
        RET
