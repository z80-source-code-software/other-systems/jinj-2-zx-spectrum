; ASM source file created by SevenuP v1.12
; SevenuP (C) Copyright 2002-2004 by Jaime Tejedor G�mez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16,   8)
;Char Size:       (  2,   1)
;Sort Priorities: X char, Char line
;Attributes:      At the end
;Mask:            No
;         org 32768
;----------------------------------------------------------------
;       fila x columnas
        ORG 24080
       DEFB 0,48,0
       DEFB 0,48,8
       DEFB 0,48,16
       DEFB 0,48,28
       DEFB 1,80,0
       DEFB 1,80,8
       DEFB 0,80,20
       DEFB 0,80,24
       DEFB 0,112,4
       DEFB 0,112,12
       DEFB 1,112,20
       DEFB 0,112,28
       DEFB 0,144,0
       DEFB 0,144,8
       DEFB 0,144,16
       DEFB 0,144,20
       DEFB 1,48,4
       DEFB 1,48,12
       DEFB 1,48,20
       DEFB 1,48,24
       DEFB 0,80,4
       DEFB 0,80,12
       DEFB 1,80,16
       DEFB 1,80,28
       DEFB 1,112,0
       DEFB 1,112,8
       DEFB 0,112,16
       DEFB 1,112,24
       DEFB 1,144,4
       DEFB 1,144,12
       DEFB 1,144,24
       DEFB 1,144,28
       DEFB 128
