;RUTINA DE COMPROBACION DE DIRECCIONES DEL PROTA
;################################################

REST_FONDOS:                        ;Restauración de fondos.----------------
               CALL C_DIRBUFFSPATT  ;HL=direcc.del fondo en buffer atrib.
               CALL VOLCARSPATTOTAL ;En vocaratt.asm Vuelca el color del fondo
               call Avolcamiento
               RET
BUSCOR_HOR     LD DE,06
               LD A,(DIRDMOV)
               cp 1
               JR NZ,BUSCOR_HORD
               LD HL,TABCOPROHI
               ld a,(FRAME)
               ld b,a
sibuiz         ld a,(hl)
               cp b
               jr z,encon_corhi
               add hl,de
               jr sibuiz
encon_corhi
               inc hl
               ld a,(hl)
               ld (PROIZ),a
               inc hl
               ld a,(hl)
               ld (PROIZ+1),a
               inc hl
               ld a,(hl)
               ld (PRODE),a
               inc hl
               ld a,(hl)
               ld (PRODE+1),a
               INC HL
               LD A,(HL)
               LD (FRAME),A
               ret
BUSCOR_HORD:
               LD HL,TABCOPROHD
               ld a,(FRAME)
               ld b,a
sibude         ld a,(hl)
               cp b
               jr z,encon_corhd
               add hl,de
               jr sibude
encon_corhd    inc hl
               ld a,(hl)
               ld (PRODE),a
               inc hl
               ld a,(hl)
               ld (PRODE+1),a
               inc hl
               ld a,(hl)
               ld (PROIZ),a
               inc hl
               ld a,(hl)
               ld (PROIZ+1),a
               INC HL
               LD A,(HL)
               LD (FRAME),A
               ret

BUSCOR_VER     LD DE,06
               LD A,(DIRDMOV)
               CP 3
               JR Z,BUSCOVERAB
BUSCOVERAR
               LD HL,TABCOPROVAR
               ld a,(FRAMEV)
               ld b,a
sibuv          ld a,(hl)
               cp b
               jr z,encon_corv
               ADD HL,DE
               jr sibuv
encon_corv     inc hl
               ld a,(hl)
               ld (PROAR),a
               inc hl
               ld a,(hl)
               ld (PROAR+1),a
               inc hl
               ld a,(hl)
               ld (PROAB),a
               inc hl
               ld a,(hl)
               ld (PROAB+1),a
               INC HL
               LD A,(HL)
               LD (FRAMEV),A
               RET
BUSCOVERAB
               LD HL,TABCOPROVAB
               ld a,(FRAMEV)
               ld b,a
sibuvb         ld a,(hl)
               cp b
               jr z,encon_corvb
               ADD HL,DE
               jr sibuvb
encon_corvb    inc hl
               ld a,(hl)
               ld (PROAB),a
               inc hl
               ld a,(hl)
               ld (PROAB+1),a
               inc hl
               ld a,(hl)
               ld (PROAR),a
               inc hl
               ld a,(hl)
               ld (PROAR+1),a
               INC HL
               LD A,(HL)
               LD (FRAMEV),A
               RET

;TABLAS DE CORRESPONDENCIA DEL SPRITE PROTAGONISTA
TABCOPROHD
               DB #00,#28,#EB,#E8,#EC,#00
               DB #01,#68,#EB,#48,#EE,#04
               DB #02,#C8,#EB,#E8,#ED,#03
               DB #03,#28,#EC,#88,#ED,#02
               DB #04,#88,#EC,#28,#ED,#01
TABCOPROHI
               DB 0,#E8,#EC,#28,#EB,0
               DB 1,#28,#ED,#88,#EC,4
               DB 2,#88,#ED,#28,#EC,3
               DB 3,#E8,#ED,#C8,#EB,2
               DB 4,#48,#EE,#68,#EB,1
TABCOPROVAB
	       DB #00,#68,#F0,#A8,#EE,#00
               DB #01,#A8,#F0,#08,#F0,#04
               DB #02,#08,#F1,#A8,#EF,#03
               DB #03,#68,#F1,#48,#EF,#02
               DB #04,#C8,#F1,#E8,#EE,#01
TABCOPROVAR
               DB 00,#A8,#EE,#88,#F1,00
               DB 01,#E8,#EE,#C8,#F1,05
               DB 02,#48,#EF,#68,#F1,04
               DB 03,#A8,#EF,#08,#F1,03
               DB 04,#08,#F0,#A8,#F0,02
